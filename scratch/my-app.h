#ifndef MY_APP_H
#define MY_APP_H

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "routing-stats.h"

namespace ns3{
  //////////////////////////////////////////////
  ///////    class   declaration /////////
  /////////////////////////////////////////////

  class MyApp : public Application
  {
  public:
    MyApp ();
    virtual ~MyApp ();

    /**
     * Register this type.
     * \return The TypeId.
     */
     static TypeId GetTypeId (void);
     void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate);
     RoutingStats& Getstats();
     void resumeApp();
     void pauseApp();
     bool isRunning();
     void busyWaiting();
     uint32_t getResiduePkts ();
  private:

    virtual void StartApplication (void);
    virtual void StopApplication (void);

    void ScheduleTx (void);
    void SendPacket (void);

    Ptr<Socket>     m_socket;
    Address         m_peer;
    uint32_t        m_packetSize;
    uint32_t        m_nPackets;
    DataRate        m_dataRate;
    EventId         m_sendEvent;
    bool            m_running;
    uint32_t        m_packetsSent;


///////////////////////////////////////////////////////////////////////////////
///////////        statistics          /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

    RoutingStats stats;

  };

/////////////////////////////////////////////
  ///////    class   definition /////////
  /////////////////////////////////////////////
  MyApp::MyApp ()
  : m_socket (0),
  m_peer (),
  m_packetSize (0),
  m_nPackets (0),
  m_dataRate (0),
  m_sendEvent (),
  m_running (false),
  m_packetsSent (0)
  {
  }

  MyApp::~MyApp ()
  {
    m_socket = 0;
  }

  /* static */
  TypeId MyApp::GetTypeId (void)
  {
    static TypeId tid = TypeId ("MyApp")
    .SetParent<Application> ()
    .SetGroupName ("Tutorial")
    .AddConstructor<MyApp> ()
    .AddAttribute ("Running",
                   "The states of packet delivery.",
                   BooleanValue(true),
                   MakeUintegerAccessor (&MyApp::m_running),
                   MakeBooleanChecker())
    ;
    return tid;
  }

  void
  MyApp::Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate)
  {
    m_socket = socket;
    m_peer = address;
    m_packetSize = packetSize;
    m_nPackets = nPackets;
    m_dataRate = dataRate;
    m_running = false;
  }

  void
  MyApp::StartApplication (void)
  {
    
    m_packetsSent = 0;
    if (InetSocketAddress::IsMatchingType (m_peer))
    {
      m_socket->Bind ();
  }
  else
  {
      m_socket->Bind6 ();
  }
  m_socket->Connect (m_peer);
  SendPacket ();
  }

  void
  MyApp::StopApplication (void)
  {
    m_running = false;

    if (m_sendEvent.IsRunning ())
    {
      Simulator::Cancel (m_sendEvent);
  }

  if (m_socket)
  {
      m_socket->Close ();
  }
  }
  
  void
  MyApp::SendPacket (void)
  { 
    if (m_running)
    {
          Ptr<Packet> packet = Create<Packet> (m_packetSize);
          m_socket->Send (packet);
          stats.IncTxBytes(packet->GetSize());
          stats.IncTxPkts();
          ScheduleTx ();
          // if (++m_packetsSent < m_nPackets)
          // {
          //   ScheduleTx ();
          // }
          // else
          // {
          //   NS_LOG_UNCOND ("APP COMPLETE!!!!");
          // }

    }
    else
    {
         busyWaiting ();
    }

  }

  void
  MyApp::ScheduleTx (void)
  {
    if (m_running)
    {
      Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ())));
      m_sendEvent = Simulator::Schedule (tNext, &MyApp::SendPacket, this);
    }
    else
    {
  
      // stats.SetTxBytes(0);
      // stats.SetTxPkts(0);
      busyWaiting();
    }
  }
  RoutingStats& MyApp::Getstats()
  {
    return stats;
  }

}
uint32_t MyApp::getResiduePkts ()
{
  return m_nPackets - m_packetsSent;
}

void MyApp::resumeApp()
{
  m_running = true;
}

void MyApp::pauseApp()
{
  m_running = false;
}
bool MyApp::isRunning()
{
  return m_running;
}

void MyApp::busyWaiting()
{
  Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ())));
  Simulator::Schedule (tNext, &MyApp::ScheduleTx, this);
}
#endif