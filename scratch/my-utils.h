#ifndef MY_UTILS_H
#define MY_UTILS_H
#include "my-macro.h"
#include <sys/stat.h>
#include <unistd.h>
using namespace std;
///////////////////////////////////////////////////
/// some utilies  made by myself
/// none of bussiness of ns3
//////////////////////////////////////////
std::vector<std::string> split_string(std::string str, char delimiter)
{
	std::stringstream ss(str);
	std::string item = "";
	std::vector<std::string> context_vector;
	while(std::getline(ss, item, delimiter))
	{
		context_vector.push_back(item);
	}
	return context_vector;
}
uint32_t getNodeIndexFromContext (std::string context)
{
	// the application index is at 3rd slots of the array, so the index is 2
	std::stringstream node_idx_str (split_string (context, '/')[2]);
	uint32_t node_idx = 0;
	node_idx_str >> node_idx;
	return node_idx;
}
uint32_t getAppIndexFromContext (std::string context)
{
	// the application index is at 5th slots of the array, so the index is 4
	std::stringstream node_idx_str (split_string (context, '/')[4]);
	uint32_t node_idx = 0;
	node_idx_str >> node_idx;
	return node_idx;
}
int atoi_for_string(std::string str)
{
	std::stringstream ss(str);
	int result;
	ss >> result;
	return result;
}
bool isFileExist (const std::string& name) 
{
		struct stat buffer= {0};   
		return (stat (name.c_str(), &buffer) == 0); 
}

int getLastDirNameInt (string root_path)
{
	struct stat st = {0};
	ostringstream this_dir;
	int dir_idx = 1; 
	do 
	{
		this_dir.str ("");
		this_dir <<root_path<< dir_idx ++<< "/";
		cout<<"getLastDirNameInt/debug:" << this_dir.str ().c_str () << endl;
	} while  (stat (this_dir.str ().c_str (), &st) == 0);
	cout << "dir_idx: " << dir_idx << endl;
	return (dir_idx-2);
}

string getLastDirNameStr (string root_path)
{
	ostringstream oss;
	oss << int (getLastDirNameInt (root_path)) ;
	return oss.str ();
}
void makeLogDir (string path)
{
	struct stat st = {0};

	if (stat(path.c_str (), &st) == -1) 
	{
	    mkdir(path.c_str (), 0700);
	}

}
#endif