#ifndef MY_RSU_STATUS_H
#define MY_RSU_STATUS_H
class RSUStatus
{
public:
	RSUStatus (int time_num, int rsu_num)
	{
		retrans_list = vector<vector<double> > (time_num, std::vector<double> (rsu_num, -1.0));

	}
	~RSUStatus ()
	{

	}
	double getRetransTimes (int time_idx, uint32_t rsu_idx)
	{
		return retrans_list[time_idx][static_cast<int> (rsu_idx)];
	}
	void setRetransTimes (int time_idx, uint32_t rsu_idx, double retrans)
	{
		retrans_list[time_idx] [static_cast<int> (rsu_idx)] =  retrans;
	}
	
private:
	vector<vector<double> > retrans_list;	

	int rsu_num;
};
#endif