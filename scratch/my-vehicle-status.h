#ifndef MY_VEHICLE_STATUS_H
#define MY_VEHICLE_STATUS_H
class VehicleStatus
{
public:
	VehicleStatus (int vehicle_num)
	{
		pos_list = std::vector<Vector> (vehicle_num);
		velo_list = std::vector<double> (vehicle_num);
	}
	~VehicleStatus ()
	{

	}
	Vector getPosition (int node_idx)
	{
		return pos_list[node_idx];
	}
	void setPosition (int node_idx, Vector v)
	{
		pos_list[node_idx] = v;
	}
	double getVelocity (int node_idx)
	{
		return velo_list[node_idx];
	}
	void setVelocity (int node_idx, double velo)
	{
		velo_list[node_idx] = velo;
	}
private:
	std::vector<Vector> pos_list;	
	std::vector<double> velo_list;	

	int vehicle_num;
};
#endif