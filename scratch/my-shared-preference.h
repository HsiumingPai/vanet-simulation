#ifndef SHARED_PREFERENCE_H
#define SHARED_PREFERENCE_H
	class SharedPreference
	{
		// #SharedPreference
	public:
		SharedPreference ( const SharedPreference& obj)
		//copy constructor
		{
			m_ipv4 = obj.m_ipv4;
			m_dev = obj.m_dev;
			m_app = obj.m_app;	
			m_wifiMac = obj.m_wifiMac;
			m_wifiPhy = obj.m_wifiPhy;
			m_wifi = obj.m_wifi;
			name = obj.name;
			num = obj.num;
			pgw_node = obj.pgw_node;
			vehicle_nodes = obj.vehicle_nodes;
			remote_nodes = obj.remote_nodes;
			epc_helper = obj.epc_helper;
			m_remote_dev = obj.m_remote_dev;
			lte_helper = obj.lte_helper;
			m_enodeb_dev = obj.m_enodeb_dev;
			m_vehicle_lte_dev = obj.m_vehicle_lte_dev;
			m_remote_nodes_num = obj.m_remote_nodes_num;
			m_rsu_nodes = obj.m_rsu_nodes;
			m_payload = obj.m_payload;
			m_pkts_num = obj.m_pkts_num;
			m_stop_time = obj.m_stop_time ;
			m_speed_min = obj.m_speed_min ;
			m_speed_max = obj.m_speed_max ;
			m_bound = obj.m_bound ;
			m_bg_traffic = obj.m_bg_traffic;
			m_mobility_mode = obj.m_mobility_mode;
			m_send_rate = obj. m_send_rate;

		}
		SharedPreference ()
		//default constructor
		{
		}

		SharedPreference  (string n, uint32_t sz = 1, uint32_t payload = 1490, uint32_t pkts_num = 10000, double stop_time = 100.0) 
		{
			name = n;
			num = sz;
			m_payload = payload;
			m_pkts_num = pkts_num;
			m_stop_time = stop_time;
		}
		string name;
		uint32_t num;
		Ipv4AddressHelper m_ipv4;
		NetDeviceContainer m_dev;
		ApplicationContainer m_app;
		NqosWifiMacHelper m_wifiMac;
		YansWifiPhyHelper m_wifiPhy;
		WifiHelper m_wifi;
		Ptr<Node> pgw_node;
		NodeContainer vehicle_nodes;
		NodeContainer remote_nodes;
		Ptr<PointToPointEpcHelper> epc_helper;
		NetDeviceContainer m_remote_dev;
		Ptr<LteHelper> lte_helper;
		NetDeviceContainer m_enodeb_dev;
		NetDeviceContainer m_vehicle_lte_dev;
		uint32_t m_remote_nodes_num;
		NodeContainer m_rsu_nodes;
		uint32_t m_payload;
		uint32_t m_pkts_num;
		double m_stop_time;
		double m_speed_min;
		double m_speed_max;	
		double m_bound;
		bool m_bg_traffic;
		string m_mobility_mode;
		string m_send_rate;

	};
#endif