#ifndef NODE_HISTORY_H
#define NODE_HISTORY_H
#include <vector>
#include <string>
#include <algorithm> 
#include "process-data.h"
#include "my-macro.h"
#include "my-utils.h"
using namespace std;


class NodeHistory
{
	// #NodeHistory
	public:
		double init_value;
		string m_filename;
		void setupFilename (string fn)
		{
			m_filename = fn;
		}
		NodeHistory ()
		{
			init_value  = 0;
			int slot_count = (STOP_TIME / SAMPLING_FREQUENCY) + 1;
			pkt_sink_wifi_rx_bit_rates = vector<double> (slot_count, init_value);
			pkt_sink_lte_rx_bit_rates = vector<double> (slot_count, init_value);
			phy_tx_drop_rates = vector<double> (slot_count, init_value);
			phy_rx_drop_rates = vector<double> (slot_count, init_value);
			mac_tx_bit_rates = vector<double> (slot_count, init_value);
			mac_rx_bit_rates = vector<double> (slot_count, init_value);
			rsu_rx_bit_rates = std::vector<double> (slot_count, init_value);
			mac_tx_drop_rates = vector<double> (slot_count,init_value);
			mac_rx_drop_rates = vector<double> (slot_count, init_value);
			wifi_conn_num = vector<double> (slot_count, init_value);
			lte_conn_num = vector<double> (slot_count, init_value);
			rsu_connection_count = vector<double> (slot_count, init_value);

			m_filename = string("");


		}
		~NodeHistory ()
		{

		}

		string dumpDoubleData (vector<double> vec)
		{
			// bool all_unchanged = check_vector_init (vec);
			bool all_unchanged = false;
			if (all_unchanged)
			{
				return "no data";
			}
			else 
			{
				// int last_index = get_last_valid_index (vec);
				ostringstream oss;
				for (vector<double>::iterator it = vec.begin (); it != vec.end (); ++it)
				{
					oss << *it << ",";
				}
				return  oss.str (). c_str ();
			}
		}

		// utility of vector<NodeHistory>
		static void dumpAllNodes (vector<NodeHistory>  hist, double duration, uint32_t vehicle_num)
		{
			stringstream oss;
			oss << render_time_sequence (duration, SAMPLING_FREQUENCY) << endl;
			
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "pkt sink wifi rx bit rates " << it - hist.begin() << "," << it->dumpDoubleData (it->getPktSinkWifiRxBitRates ()) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "pkt sink lte rx bit rates " << it - hist.begin() << "," << it->dumpDoubleData (it->getPktSinkLteRxBitRates ()) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac rx bit rates " << it - hist.begin() << "," << it->dumpDoubleData (it->getMacRxBitRates ()) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac tx bit rates " << it - hist.begin() << "," << it->dumpDoubleData (it->getMacTxBitRates ()) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "phy tx drop " << it - hist.begin() << "," << it->dumpDoubleData (it->getPhyTxDropRates ()) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "phy rx drop " << it - hist.begin() << "," << it->dumpDoubleData (it->getPhyRxDropRates ()) << endl;				
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac tx drop " << it - hist.begin() << "," << it->dumpDoubleData (it->getMacTxDropRates ()) << endl;				
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac rx drop " << it - hist.begin() << "," << it->dumpDoubleData (it->getMacRxDropRates ()) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
 			{
 				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "RSU Rx bits " << it - hist.begin() << "," << it->dumpDoubleData (it->getRSURxRates ()) << endl;
 			}

 			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
 			{
 				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "RSU Connection Count " << it - hist.begin() << "," << it->dumpDoubleData (it->getRSUConnectionCount ()) << endl;
 			}
			// NS_LOG_UNCOND (oss.str ().c_str ());
			hist[0].writeFile ( hist[0].m_filename+string("_log"), oss.str ());
			dumpProcessdData (hist, duration, vehicle_num);
		}

		static void dumpProcessdData (vector<NodeHistory>  hist, double duration, uint32_t vehicle_num)
		{
			stringstream oss;
			oss << render_time_sequence (duration, 1.0) << endl;
			int resampling_size = 10;				
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "pkt sink wifi rx bit rates " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getPktSinkWifiRxBitRates (), resampling_size)) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "pkt sink lte rx bit rates " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getPktSinkLteRxBitRates (), resampling_size)) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac rx bit rates " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getMacRxBitRates (), resampling_size)) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac tx bit rates " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getMacTxBitRates (), resampling_size)) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "phy tx drop " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getPhyTxDropRates (), resampling_size)) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "phy rx drop " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getPhyRxDropRates (), resampling_size)) << endl;				
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac tx drop " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getMacTxDropRates (), resampling_size)) << endl;				
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
			{
				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "mac rx drop " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getMacRxDropRates (), resampling_size)) << endl;
			}

			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
 			{
 				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "rsu rx " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getRSURxRates (), resampling_size)) << endl;
			}
			for (vector<NodeHistory>::iterator it=hist.begin() ; it != hist.end (); ++it)
 			{
 				// NS_LOG_UNCOND( "id " << it - hist.begin() << ":");
				oss << "RSU Connection Count " << it - hist.begin() << "," << it->dumpDoubleData ( ProcessLog::groupSamplingPoint(it->getRSUConnectionCount (), resampling_size)) << endl;
			}

		
			// NS_LOG_UNCOND (oss.str ().c_str ());
			hist[0].writeFile ( hist[0].m_filename+ string("_proc"), oss.str ()  );
		}
	
		void IncPktSinkLteRx (uint32_t pkt_unit = 1)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getPktSinkLteRxBitRates() [time_index] += pkt_unit * 8;
		}
		void IncPktSinkWifiRx (uint32_t pkt_unit = 1)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getPktSinkWifiRxBitRates() [time_index] += pkt_unit * 8;
		}		
		
		void IncMacRx (uint32_t pkt_unit = 1)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getMacRxBitRates() [time_index] += pkt_unit * 8;
		}
		void IncRSURx (uint32_t pkt_unit = 1)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getRSURxRates() [time_index] += pkt_unit * 8;
		}
		void IncMacTx (uint32_t pkt_unit = 1)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getMacTxBitRates() [time_index] += pkt_unit * 8;
		}
		void IncPhyTxDrop ()
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getPhyTxDropRates()[time_index] += 1;
		}
		void IncPhyRxDrop ()
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getPhyRxDropRates() [time_index] += 1;

		}
		void IncMacTxDrop ()
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getMacTxDropRates() [time_index] += 1;
		}

		void SetRSUConnectionCount (double num)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getRSUConnectionCount() [time_index] = num;
		}

		void IncMacRxDrop ()
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getMacRxDropRates ()[time_index] += 1;

		}

		void LogWifiConnectionNumber (double num)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getWifiConnectionNumber ()[time_index] = num;
		}
		void LogLteConnectionNumber (double num)
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) ;
			getLteConnectionNumber ()[time_index] = num;
		}

		vector<double>& getPktSinkLteRxBitRates ()
		{
			return pkt_sink_lte_rx_bit_rates;	
		}
		vector<double>& getPktSinkWifiRxBitRates ()
		{
			return pkt_sink_wifi_rx_bit_rates;	
		}

		vector<double>& getMacRxBitRates ()
		{
			return mac_rx_bit_rates;	
		}
		vector<double>& getMacTxBitRates ()
		{
			return mac_tx_bit_rates;
		}
		
		vector<double>& getPhyTxDropRates ()
		{
			return phy_tx_drop_rates;
		}
		
		vector<double>& getPhyRxDropRates ()
		{
			return phy_rx_drop_rates;
		}
		
		vector<double>& getMacTxDropRates ()
		{
			return mac_tx_drop_rates;
		}
		vector<double>& getMacRxDropRates ()
		{
			return mac_rx_drop_rates;
		}
		std::vector<double>& getRSURxRates ()
		{
			return rsu_rx_bit_rates;
		}
 
		vector<double>& getWifiConnectionNumber ()
		{
			return wifi_conn_num;;
		}
		vector<double>& getLteConnectionNumber ()
		{
			return lte_conn_num;
		}
		vector<double>& getRSUConnectionCount ()
		{
			return rsu_connection_count;
		}


		static string  gettime ()
		{
			time_t t = time (0);
			struct tm * now = localtime( & t );
			ostringstream oss;
			oss << (now->tm_year + 1900) << '-' 
			<< (now->tm_mon + 1) << '-'
			<< now->tm_mday << '-'
			<< now->tm_hour<< '-'
			<< now->tm_min<< '-'
			<< now->tm_sec;
			return oss.str ();
		}
		static string mygetdate ()
		{
			time_t t = time (0);
			struct tm * now = localtime( & t );
			ostringstream oss;
			oss << (now->tm_year + 1900) << '-' 
			<< (now->tm_mon + 1) << '-'
			<< now->tm_mday;
			return oss.str ();

		}


	private:
		static string render_time_sequence (double duration, double sampling_frequency)
		{
			ostringstream oss;
			oss << "Event Node,";
			// NS_LOG_UNCOND ("duration="<<duration<<",sampling_frequency="<<sampling_frequency<<",r/s="<<static_cast<uint32_t>((duration)/sampling_frequency+1));
			for (uint32_t i = 0; i < static_cast<uint32_t>((duration)/sampling_frequency+1); ++i)
			{
				oss << static_cast<double> (i) * sampling_frequency << ",";					
			}
			return oss.str();
		}
		int get_last_valid_index (vector<double> v)
		{
			for (std::vector<double>::iterator it = v.end() - 1; it != v.begin(); --it)
			{
				if (*it != init_value)
					return it - v.begin ();
			}
			return v.size ();
		}
		bool check_vector_init(vector<double>  vec)
		{
			for (std::vector<double>::iterator it = vec.begin (); it != vec.end (); ++it)
			{
				if (*it != init_value)
				{
					return false;
				}
			}
			return true;
		}
		
		void writeFile (string filename,string content)
		{		
			string root_path ("log/");
			string path_dir (mygetdate ()+string ("/"));
			ostringstream oss;
			oss << int (getLastDirNameInt(root_path+path_dir));
			NS_LOG_UNCOND ("writeFile/debug: " << oss.str ());
			path_dir =root_path + path_dir + oss.str ()+ string ("/");
			oss.str ("");
			oss <<  path_dir.c_str () <<filename.c_str () << gettime () << ".csv";
			
			ofstream fout (oss.str ().c_str (), ofstream::app);
			// cout <<  "write log : " << oss.str ().c_str () << endl;
			// cout << (content) << endl;
			NS_ASSERT_MSG (fout, "writing file fails," << oss.str ());
			fout << content << endl;	
			fout.close ();
		}
		
		vector<double> mac_rx_bit_rates;
		vector<double> mac_tx_bit_rates;
		vector<double> pkt_sink_wifi_rx_bit_rates;
		vector<double> pkt_sink_lte_rx_bit_rates;
		vector<double> rsu_rx_bit_rates;
		vector<double> phy_tx_drop_rates;
		vector<double> phy_rx_drop_rates;
		vector<double> mac_tx_drop_rates;
		vector<double> mac_rx_drop_rates;
		vector<double>  lte_conn_num;
		vector<double>  wifi_conn_num;
		vector<double> rsu_connection_count;


};
#endif