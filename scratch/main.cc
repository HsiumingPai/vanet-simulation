#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"

#include "ns3/applications-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/config-store-module.h"
#include "ns3/integer.h"

#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/names.h"
#include "ns3/netanim-module.h"

#include "ns3/ipv4-l3-protocol.h"

#include <vector>
#include <sstream>
#include <string>
#include <cstdio>
#include <fstream>
#include <iostream>
#include "my-utils.h"
#include "my-app.h"
#include "my-node-history.h"
#include "my-shared-preference.h"
#include "my-node-factory.h"
#include "my-vehicle-status.h"
#include "my-rsu-status.h"
#include <ctime>
#include "ns3/stats-module.h"
#include <map>
#include <algorithm>
#include <utility>
#include <cmath>
#include "my-macro.h"
#include <algorithm>
// NS_LOG_COMPONENT_DEFINE ("my-test");


using namespace ns3;
using namespace std;

bool pairCompare (const pair<uint32_t, uint32_t > & ele1, const pair<uint32_t, uint32_t > & ele2 )
{
	return ele1.second > ele2.second;
}

class MyTest
{
public:	
	MyTest ()
	{
		// #initialization of nodes group
		m_retrans_threshold = 1;
		m_vehicle_speed_min = 0;
		m_vehicle_speed_max = 14;

		m_vehicle_num = 20;

		uint32_t probe_num = 0;
		m_rsu_num = 5;
		m_retrans_lower_bound = 0.25;

		uint32_t payload = 1490;
		uint32_t pkts_num = 400000000;
		uint32_t enodeb_num = 1;
		m_coverage_threshold = 250;

		m_stop_time = STOP_TIME;
		m_sampling_frequency = SAMPLING_FREQUENCY;
		m_simulator_stop_time = m_stop_time;
		m_simulation_duration = m_stop_time;
		m_node_group.push_back (new SharedPreference ("vehicle", m_vehicle_num));
		m_node_group.push_back (new SharedPreference ("remote host", m_vehicle_num, payload, pkts_num, m_stop_time));
		m_node_group.push_back (new SharedPreference ("rsu", m_rsu_num));
		m_node_group.push_back (new SharedPreference ("enodeb", enodeb_num));
		m_node_group.push_back (new SharedPreference ("pgw", 1, payload, pkts_num));
		m_node_group.push_back (new SharedPreference ("probe node", probe_num * 2, payload, pkts_num, m_stop_time));
		m_is_logging = false;
		m_is_debug = false;
		m_my_algorithm_on = true ;
		start_time = 0.;
		m_cw = new ContentionWindowLog ( static_cast<float>((m_stop_time)/SAMPLING_FREQUENCY )+ 1, m_vehicle_num);
		cw = std::vector<uint32_t> (m_vehicle_num);
		m_vehicle_status = new VehicleStatus (m_vehicle_num);

		ostringstream oss;
		oss << "with_algo_th" << m_retrans_threshold << "_";
		m_log_filename = oss.str ();
		

		m_rsu_status = new RSUStatus ((m_stop_time/SAMPLING_FREQUENCY) + 1 , m_rsu_num);
		m_rsu_connection_count = std::vector<double> (m_rsu_num, 0.0);
		m_mobility_mode = string ("randomDisk");
		m_send_rate = string ("1Mbps");

	}
	~MyTest ()
	{
	}
	// #config
	void setRetransmissionThreshold (double thresh)
	{
		NS_LOG_UNCOND ("setup retransmission threshold = " << thresh);
		m_retrans_threshold = thresh;
	}
	void setSpeedMin (double v)
	{
		NS_LOG_UNCOND ("setup vehicles velocity = " << v);
		m_vehicle_speed_min = v;
	}
	void setSpeedMax (double v)
	{
		NS_LOG_UNCOND ("setup vehicles velocity = " << v);
		m_vehicle_speed_max = v;
	}
	void setBound (double bd)
	{
		NS_LOG_UNCOND ("setup boundary =" << bd);
		m_vehicle_bound = bd/2.0;
	}
	void setVehicleNumber (uint32_t v)
	{
		NS_LOG_UNCOND ("setup vehicle number = " << v);
		m_vehicle_num = v;
	}
	void setRSUNumber (uint32_t r)
	{
		NS_LOG_UNCOND ("setup rsu number = " << r );
		m_rsu_num = r;
	}
	void setBackgroundTraffic (bool bg)
	{
		if (bg)
		{
			NS_LOG_UNCOND ("turn on BackgroundTraffic");
		}
		else
		{
			NS_LOG_UNCOND ("turn off BackgroundTraffic");
		}
		
		m_vehicle_background_traffic = bg;
	}
	void setCoverageRadius (double r)
	{
		NS_LOG_UNCOND ("set coverage radius = " << r);
		m_coverage_threshold = r;
	}
	void setupAlgorithm (bool algo = true)
	{
		if (algo)
		{
			NS_LOG_UNCOND ("algorithm have been set on");
		}
		else
		{
			NS_LOG_UNCOND ("algorithm have been set off");
		}
		m_my_algorithm_on = algo;
	}
	void setupExportCSV(bool e)
	{
		if (e)
		{
			NS_LOG_UNCOND ("setup Export CSV");
		}
		else
		{
			NS_LOG_UNCOND ("No CSV Exported");
		}
		m_is_logging = e;		
	}
	void setupDebug(bool d)
	{
		if (d)
		{
			NS_LOG_UNCOND ("debug message is on");
		}
		else
		{
			NS_LOG_UNCOND ("debug message is off");
		}
		m_is_debug = d;
	}
	void setupExportCSVName (string name)
	{
		m_log_filename = name;
	}

	void setupMobilityMode (string mob)
	{
		NS_LOG_UNCOND ("set mobility to " << mob);
		m_mobility_mode = mob;
	}

	void setupSendingRate (string rate)
	{
		NS_LOG_UNCOND ("set send rate to " << rate);
		m_send_rate = rate;
	}	
	// `
	bool str_to_bool (string token)
	{
		return token == string("true") ? true:false;
	}


	void readConfig (string path)
	{
		NS_LOG_UNCOND ("LOAD config from " << path);
		ifstream fin(path.c_str ());
		ostringstream oss;
		oss << "CFG file read fail at " << path << endl;
		NS_ASSERT_MSG (fin, oss.str ().c_str ());
		string buf;
		map<string, string> cfg_map;
		//read and store the config
		while (getline (fin, buf))
		{
			istringstream iss (buf);
			string buf1, buf2;
			iss >> buf1 >> buf2;
			if ( buf1.size () == 0 && buf1.size () == buf2.size ())
			{
				continue;
			}
			cfg_map[buf1] = buf2;
			
			NS_ASSERT_MSG (buf2.size () > 0, buf1.c_str () << "have no value");
		}
		//setup the config
		for (std::map<string, string>::iterator it = cfg_map.begin(); it != cfg_map.end(); ++it)
		{
			istringstream  iss;
			iss.str (it->second);
			if (it->first == string ("RetransmissionTimesThreshold"))
			{
				double buf;
				iss >> buf;
				setRetransmissionThreshold (buf);
			}
			else if (it->first == string ("AlgorithmOn"))
			{
				bool buf;
				buf = str_to_bool (it->second);
				setupAlgorithm (buf);
			}
			else if (it->first == string ("SpeedMin"))
			{
				double buf;
				iss >> buf;
				setSpeedMin (buf);
			}
			else if (it->first == string ("SpeedMax"))
			{
				double buf;
				iss >> buf; 
				setSpeedMax (buf);
			}
			else if (it->first == string ("PathLength"))
			{
				double buf;
				iss >> buf; 
				setBound (buf);
			}
			else if (it->first == string ("VehicleNumber"))
			{
				uint32_t buf;
				iss >> buf;
				setVehicleNumber (buf);
			}
			else if (it->first == string ("RSU_Number"))
			{
				uint32_t buf; 
				iss >> buf;
				setRSUNumber (buf);
			}
			else if (it->first == string ("BackgroundTraffic"))
			{
				// not implement
				bool buf; 
				buf = str_to_bool (it->second);
				setBackgroundTraffic (buf);
			}
			else if (it->first == string ("CoverageRadius"))
			{
				double buf;
				iss >> buf;
				setCoverageRadius (buf);
			}
			else if (it->first == string ("ExportCSV"))
			{
				bool buf;
				buf = str_to_bool (it->second);
				setupExportCSV(buf);
			}
			else if (it->first == string ("IsDebug"))
			{
				bool buf;
				buf = str_to_bool (it->second);
				setupDebug(buf);
			}
			else if (it->first == string ("CSVName"))
			{
				setupExportCSVName (it->second);
			}
			else if (it->first == string ("MobilityModel"))
			{
				setupMobilityMode (it->second);
			}
			else if (it->first == string ("SendRate"))
			{
				setupSendingRate (it->second);
			}

				
		}
	}
	
	virtual void parseCmd (int argc, char* argv[])
	{
		CommandLine cmd;
		// cmd.AddValue ("debug", "Total duration of the simulation (in seconds)", m_is_debug);
		// cmd.AddValue ("log", "Speed of the UE (default = 20 m/s)", m_is_logging);
  		  

		cmd.Parse(argc, argv);
		// enable rts cts all the time.
		Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2200"));
		// disable fragmentation
		Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
		Config::SetDefault ("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (80));

	}


	virtual void deployEnvironment ()
	{
		for (vector<SharedPreference*>::iterator it = m_node_group.begin(); it != m_node_group.end(); ++it)
		{
			cout << (*it)->name << endl;
			NodeFactory* helper = createHelper ((*it)->name);
			helper->SetupMobility ();
			helper->SetupNetDev ();
			helper->SetupIP ();
			helper->SetupApp ();
		}
		// Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
	}
	
	void disconnectWifiActive (uint32_t node_idx)
	{
		// mac addr is belong to RSU which the node connect to
		if (type_node (node_idx) == "vehicle") 
		{	
			setWifiSleep (node_idx);
			int rsu_idx = getRSUOfVehicle (node_idx);
			
			if (0 != rsu_idx)
			{
				m_rsu_connection_count[(rsu_idx- 2 * m_vehicle_num)] -= 1;		
			}

			
						// index 2 is lte recv and 3 is the lte send
			// index 0 is wifi recv and 1 is the wifi recv
			//when the vehicle trigger the event, I have to close the remote host (because I cannot close the packet sink on vehicle)
			Ptr<Node> remote_node = NodeList::GetNode (getRemoteHostIndex (node_idx));
			Ptr<Node> vehicle_node = NodeList::GetNode (node_idx);

			// Ptr<MyApp> remote_send_Lte_app = DynamicCast<MyApp> (remote_node->GetApplication (3));
			Ptr<MyApp> vehicle_send_Lte_app = DynamicCast<MyApp> (vehicle_node->GetApplication (1));
			
			// Ptr<MyApp> remote_send_wifi_app = DynamicCast<MyApp> (remote_node->GetApplication (1));
			Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));

			// remote_send_wifi_app->pauseApp ();
			vehicle_send_wifi_app->pauseApp ();

			// remote_send_Lte_app->resumeApp ();
			vehicle_send_Lte_app->resumeApp ();
	
		}
	}

	uint32_t getRemoteHostIndex (uint32_t node_idx)
	{
		return node_idx + m_vehicle_num;
	}
	virtual void disconnectWifi (string context, Mac48Address value)
	{
		// NS_LOG_UNCOND (context);
		// mac addr is belong to RSU which the node connect to
		uint32_t node_idx = getNodeIndexFromContext (context);
		


		if (type_node (node_idx) == "vehicle") 
		{
			// index 2 is lte recv and 3 is the lte send
			// index 0 is wifi recv and 1 is the wifi recv
			//when the vehicle trigger the event, I have to close the remote host (because I cannot close the packet sink on vehicle)
			Ptr<Node> remote_node = NodeList::GetNode (getRemoteHostIndex (node_idx));
			Ptr<Node> vehicle_node = NodeList::GetNode (node_idx);

			// Ptr<MyApp> remote_send_Lte_app = DynamicCast<MyApp> (remote_node->GetApplication (3));
			Ptr<MyApp> vehicle_send_Lte_app = DynamicCast<MyApp> (vehicle_node->GetApplication (1));
			
			// Ptr<MyApp> remote_send_wifi_app = DynamicCast<MyApp> (remote_node->GetApplication (1));
			Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));

			// remote_send_wifi_app->pauseApp ();
			vehicle_send_wifi_app->pauseApp ();

			// remote_send_Lte_app->resumeApp ();
			vehicle_send_Lte_app->resumeApp ();
	

				
		}
	}
	virtual void connectWifi (string context, Mac48Address value)
	{
		// mac addr is belong to RSU which the node connect to
		 // NS_LOG_UNCOND (context);
		uint32_t node_idx = getNodeIndexFromContext (context);
		// get the CN node (because we cannot control the receiver but sender)
		bool is_phy_rx_good = testNetworkQuality (node_idx);
		// NS_LOG_UNCOND ("the node is "<<node_idx<< ", dist = "<< getDistance (node_idx, rsu_index));
		if (is_phy_rx_good)
		{
			NS_LOG_UNCOND ("node " << node_idx << ", " << "network is good" ) ;
		}
		else
		{
			NS_LOG_UNCOND ("node " << node_idx << ", " << "network is bad" ) ;
		}
		if (type_node (node_idx) == "vehicle" && is_phy_rx_good) 
		{
			
			// index 1 is the lte application
			// index 0 is the wifi application
			//when the vehiclde trigger the event, I have to close the remote host (because I cannot close the packet sink on vehicle)
			NS_LOG_UNCOND ("CONNECT WIFI: stop remote_node " << node_idx  + m_vehicle_num );
			Ptr<Node> remote_node = NodeList::GetNode (getRemoteHostIndex (node_idx));
			Ptr<Node> vehicle_node = NodeList::GetNode (node_idx);

			// Ptr<MyApp> remote_send_Lte_app = DynamicCast<MyApp> (remote_node->GetApplication (3));
			Ptr<MyApp> vehicle_send_Lte_app = DynamicCast<MyApp> (vehicle_node->GetApplication (1));
			
			// Ptr<MyApp> remote_send_wifi_app = DynamicCast<MyApp> (remote_node->GetApplication (1));
			Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));
			// NS_LOG_DEBUG ("Assoc " << context << endl << value << endl << "number of all apps : " << NodeList::GetNode (node_idx)->GetNApplications () << lteApp);

			// remote_send_Lte_app->pauseApp ();
			vehicle_send_Lte_app ->pauseApp ();

			// remote_send_wifi_app->resumeApp ();
			vehicle_send_wifi_app ->resumeApp ();
			
			
		}

	}
	void connectWifiActive (uint32_t node_idx, uint32_t rsu_idx)
	{
		// uint32_t rsu_index = m_vehicle_num * 2;
		// bool is_phy_rx_good = testNetworkQuality (node_idx)  && getDistance (node_idx, rsu_index) < m_coverage_threshold;

		Ptr<StaWifiMac> sta_mac = getStaWifiMac (node_idx);


		ostringstream oss;
		uint32_t rsu_ssid_idx = rsu_idx -  2 * m_vehicle_num ;
		oss << "wifi" << (rsu_ssid_idx);
		NS_LOG_UNCOND ("conntect to wifi ssid :" << oss.str () << ", and channel " << rsu_ssid_idx + 1 );
		sta_mac->SetSsid (Ssid (oss.str ().c_str ()));

		if (type_node (node_idx) == "vehicle") 
		{


			setWifiWake (node_idx);
			Ptr<WifiNetDevice> wifi_dev =  getWifiNetDevice (node_idx);
			Ptr<WifiPhy> wifi_phy = wifi_dev->GetPhy ();
			wifi_phy->SetChannelNumber (rsu_ssid_idx + 1);
			// NS_LOG_UNCOND ("CONNECT WIFI: stop remote_node " << node_idx  + m_vehicle_num );
			Ptr<Node> remote_node = NodeList::GetNode (getRemoteHostIndex (node_idx));
			Ptr<Node> vehicle_node = NodeList::GetNode (node_idx);

			// Ptr<MyApp> remote_send_Lte_app = DynamicCast<MyApp> (remote_node->GetApplication (3));
			Ptr<MyApp> vehicle_send_Lte_app = DynamicCast<MyApp> (vehicle_node->GetApplication (1));
			
			
			// Ptr<MyApp> remote_send_wifi_app = DynamicCast<MyApp> (remote_node->GetApplication (1));
			Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));
			if (!vehicle_send_wifi_app->isRunning ())
			{
				m_rsu_connection_count[(rsu_idx- 2 * m_vehicle_num)] += 1;
			}
			// NS_LOG_DEBUG ("Assoc " << context << endl << value << endl << "number of all apps : " << NodeList::GetNode (node_idx)->GetNApplications () << lteApp);

			// remote_send_Lte_app->pauseApp ();
			vehicle_send_Lte_app ->pauseApp ();

			// remote_send_wifi_app->resumeApp ();
			vehicle_send_wifi_app ->resumeApp ();
			if (vehicle_send_wifi_app->isRunning ())
			{
				NS_LOG_UNCOND ("vehicle_send_wifi_app is running");	
			}


		}

	}




	void checkTime ()
	{
		
		double t =  Simulator::Now().GetSeconds();
		NS_LOG_UNCOND ("time:" << t << ",left " << (m_stop_time - t) << " sec");
		m_simulation_duration = t + m_sampling_frequency;
		
		
		Simulator::Schedule (Seconds(1.), &MyTest::checkTime, this);	
		
	}
	void MacTxDropListener (string context, Ptr<const Packet> pkt)
	{

		// NS_LOG_UNCOND ("mac tx drop" << "," << context <<",pkt=" <<*pkt);
		uint32_t node_idx = getNodeIndexFromContext (context);
		uint32_t wifi_dev = 0;
		if (NodeList::GetNode (node_idx)->GetDevice (wifi_dev)->IsLinkUp ())
		{
			history[node_idx].IncMacTxDrop ();
		}
	}
	void MacRxDropListener (string context, Ptr<const Packet> pkt)
	{
		// NS_LOG_UNCOND ("mac rx drop" << "," << context <<",pkt=" <<*pkt);
		uint32_t node_idx = getNodeIndexFromContext (context);
		uint32_t wifi_dev = 0;
		if (NodeList::GetNode (node_idx)->GetDevice (wifi_dev)->IsLinkUp ())
		{			
			history[node_idx].IncMacRxDrop ();
		}
	}
	void MacTxListener (string context, Ptr<const Packet> pkt)
	{
		
		uint32_t node_idx = getNodeIndexFromContext (context);
		
		// if (node_idx < m_vehicle_num)
		// {
		// 	NS_LOG_UNCOND ("mac tx " << "," << context << "," << *pkt);	
		// }
		uint32_t wifi_dev = 0;
		if (NodeList::GetNode (node_idx)->GetDevice (wifi_dev)->IsLinkUp ())
		{
			history[node_idx].IncMacTx (pkt->GetSize ());
		}
	}
	void MacRxListener (string context, Ptr<const Packet> pkt)
	{
		// NS_LOG_UNCOND ("mac rx " << "," << context);
		uint32_t node_idx = getNodeIndexFromContext (context);
		uint32_t wifi_dev = 0;

		if (NodeList::GetNode (node_idx)->GetDevice (wifi_dev)->IsLinkUp ())
		{
			history[node_idx].IncMacRx (pkt->GetSize ());
		}
	}
	void PktSinkRxListener (string context, Ptr<const Packet> pkt, const Address &address)
	{
		
		
		uint32_t node_idx = getNodeIndexFromContext (context);
		uint32_t app_idx = getAppIndexFromContext (context);
		// double now =  Simulator::Now ().GetSeconds ();
		// if ( now > 170.0 && app_idx == 0)
		// {
		// 	NS_LOG_UNCOND ("pkt sink rx " << "," << context << ",time: " << now );
		// }

		// uint32_t wifi_dev = 0;
		// bool IsLinkUp =NodeList::GetNode (node_idx)->GetDevice (wifi_dev)->IsLinkUp ();
		// NS_LOG_UNCOND ("pkt sink rx " << "," << context);

		if(app_idx == 0)
		{
			history[node_idx].IncPktSinkWifiRx (pkt->GetSize ());	
		}
		else if( app_idx == 1)
		{
			history[node_idx].IncPktSinkLteRx (pkt->GetSize ());	
		}
		
		
	}	
	void PhyTxDropListener (string context, Ptr<const Packet> pkt)
	{
		NS_LOG_DEBUG ("Phy Tx drop" << "," << context);
		uint32_t node_idx = getNodeIndexFromContext (context);
		uint32_t wifi_dev = 0;
		if (NodeList::GetNode (node_idx)->GetDevice (wifi_dev)->IsLinkUp ())
		{
			history[node_idx].IncPhyTxDrop ();
		}
	}

	void PhyRxDropListener (string context, Ptr<const Packet> pkt)
	{
		NS_LOG_DEBUG ("Phy Rx drop" << "," << context);
		uint32_t node_idx = getNodeIndexFromContext (context);
		uint32_t wifi_dev = 0;
		int time_index = static_cast<int> (Simulator::Now ().GetSeconds ()  / m_sampling_frequency); 
		if (NodeList::GetNode (node_idx)->GetDevice (wifi_dev)->IsLinkUp () &&  history[node_idx].getMacRxBitRates () [time_index] != 0)
		{
			history[node_idx].IncPhyRxDrop ();
		}
	}
	uint32_t getNetworkQuality (int node_idx, int time_index)
	{
		return history[node_idx].getPhyRxDropRates ()[time_index];
	}

	uint32_t sum (std::vector<uint32_t> v)
	{
		uint32_t result = 0;
		for (std::vector<uint32_t>::iterator it = v.begin(); it != v.end(); ++it)
		{
			result += *it;
		}
		return result;
	}
	double sum (std::vector<double> v)
	{
		double result = 0;
		for (std::vector<double>::iterator it = v.begin(); it != v.end(); ++it)
		{
			result += *it;
		}
		return result;
	}
	double average (std::vector<uint32_t> v)
	{
		if (v.size () == 0)
		{
			return 0.0;
		}
		else
		{
			return static_cast<double> (sum (v) )/v.size () ;
		}
	}
	double average (std::vector<double> v)
	{
		if (v.size () == 0)
		{
			return 0.0;
		}
		else
		{
			return static_cast<double> (sum (v) )/v.size () ;
		}
	}
	
	vector<uint32_t> getPhyRxDropCandidates (uint32_t time_index)
	{
		
		uint32_t threshold = 100;
		int victim_count = static_cast<int> (m_vehicle_num / 2);
		
		vector<pair<uint32_t, uint32_t> > max_rx_victims;
		for (int i = 0; i < static_cast<int> (m_vehicle_num); ++i)
		{
			max_rx_victims.push_back (make_pair (i, getNetworkQuality (i, time_index)));
			cout << "("<< max_rx_victims[i].first<<","<< max_rx_victims[i].second<<")";
		}
		sort (max_rx_victims.begin (), max_rx_victims.end (), pairCompare);
		vector<uint32_t> result_victims;
		for (int i = 0; i < victim_count; ++i)
		{
			if(max_rx_victims[i].second > threshold )
			{
				result_victims.push_back (max_rx_victims[i].first);
				cout << "victims" << endl;
				cout << "("<< max_rx_victims[i].first<<","<< max_rx_victims[i].second<<")";		
			}
			
		}
		return result_victims;
	}
	double getQuadraticFormulaSol1 (double a, double b, double c)
	{
		return (-1*b + pow ( (pow (b, 2) - 4 * a * c),1/2.0)) /(2*a);
	}
	double getQuadraticFormulaSol2 (double a, double b, double c)
	{
		return (-1*b - pow ( (pow (b, 2) - 4 * a * c),1/2.0)) /(2*a);
	}
	Vector GetDirection (Vector a, Vector b)
	{
		return Vector (b.x - a.x, b.y - a.y, 0);
	}
	double getPathLengthInCoverage (uint32_t node_idx, uint32_t rsu_idx)
	{ 
		// let the rsu is the center of coverage  (suppose coverge is circular)
		// (x-a)^2+(y-b)^2 = r^2,  there is a point (a + m * t, b + n * t) on the circular edge , (a, b) is the position of node_idx, (m, n) is a direction
		// and start from the node_idx then we can get a "vector(direction)"
		// so we can find the cross points of the line and the circle (single line have 2 cross points with a circle)
		Vector direction = GetDirection ( m_vehicle_status->getPosition (node_idx), GetPosition (node_idx));
		if (direction.x == 0 && direction.y == 0)
		{
			return 0.0;
		}
		Vector center = GetPosition (rsu_idx);		
		Vector start_point = GetPosition (node_idx);
		// NS_LOG_UNCOND ("rsu center : " << center << ", direction :" << direction << ", vehicle:" << start_point);
		double radius = m_coverage_threshold;

		double a = pow (direction.x, 2)  + pow (direction.y, 2);
		double b = 2 * direction.x * start_point.x+2*direction.y*start_point.y-2*direction.x*center.x-2*direction.y*center.y;
		double c = pow (start_point.x, 2)+pow (start_point.y, 2) - 2 * start_point.x*center.x - 2 * start_point.y * center.y+ pow (center.x, 2) + pow (center.y, 2) - pow (radius, 2);
		// parametric form 
		if ( pow (b, 2) - 4 * a * c < 0)
		{
			return 0.0; 
		}
		double t1 = max (getQuadraticFormulaSol1 (a, b, c), 0.0);
		double t2 = max (getQuadraticFormulaSol2 (a, b, c), 0.0);
		Vector cross_point1 = Vector (start_point.x + direction.x * t1, start_point.y + direction.y * t1, 0);
		Vector cross_point2 = Vector (start_point.x + direction.x * t2, start_point.y + direction.y * t2, 0);
		// NS_LOG_UNCOND ("center: " << center << ",direction:" << direction<<",start_point:" << start_point<<",cross point1: " << cross_point1<< ",cross_point2: " << cross_point2);
		return getDistance (cross_point1, cross_point2);
	}
	vector<uint32_t> getPhyRxDropAverage(int time_index)
	{	
		vector<uint32_t> phy_rx_drop_list;
		
		for (uint32_t i = m_vehicle_num; i < 2*m_vehicle_num; ++i)
		{
			if (history[static_cast<int> (i)].getPktSinkWifiRxBitRates () [time_index] != 0)
			{
				phy_rx_drop_list.push_back (getNetworkQuality ( static_cast<int> (i), time_index));
			}
			
		}
		return phy_rx_drop_list;
	}
	vector<uint32_t> getPktSinkLteRxAverage(uint32_t time_index)
	{	
		vector<uint32_t> pkt_sink_lte_rx_list;
		
		for (uint32_t i = m_vehicle_num; i < 2*m_vehicle_num; ++i)
		{	
			uint32_t lte_rx = history[i].getPktSinkLteRxBitRates ()[time_index];
			if (lte_rx != 0)
			{
				pkt_sink_lte_rx_list.push_back (lte_rx);
			}
		}
		return pkt_sink_lte_rx_list;
	}
	vector<uint32_t> getPktSinkwifiRxAverage(uint32_t time_index)
	{	
		vector<uint32_t> pkt_sink_wifi_rx_list;

		for (uint32_t i = m_vehicle_num; i < 2*m_vehicle_num; ++i)
		{	
			uint32_t wifi_rx = history[i].getPktSinkWifiRxBitRates ()[time_index];
			if (wifi_rx != 0)
			{		
				pkt_sink_wifi_rx_list.push_back (wifi_rx);
			}
		}
		return pkt_sink_wifi_rx_list;
	}
	string giveBracket (int a, double b)
	{
		ostringstream oss;
		oss << "("<< a <<","<< b <<")";
		return oss.str ();
	}
	bool testNetworkQuality (uint32_t node_idx)	
	{
		if( m_my_algorithm_on && type_node (node_idx) == "vehicle")
		{
			double now = Simulator::Now ().GetSeconds ();
			int time_index = static_cast<float> (now/SAMPLING_FREQUENCY) - 1;
			return getAverageRetransmissionTimes (time_index) < m_retrans_threshold ;
		}
		else
		{
			// cout << " no algorithm" << endl;
			return true;
		}
		// uint32_t threshold = 50;
		
		// bool result = getNetworkQuality (node_idx, time_index) < threshold;
		// result = true;
		// return result;
		
		
	}
	double getPreviousVelocity (uint32_t node_idx)
	{
		return m_vehicle_status->getVelocity (node_idx);
	}
	uint32_t getBestRSU (uint32_t node_idx)
	{
		Ptr<Node> vehicle_node = NodeList::GetNode (node_idx);
		Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));	
		if(vehicle_send_wifi_app->isRunning ())
		{
			return 0;
		}
		uint32_t result_rsu_idx = 0; 
		double max_score = 0;
		double now = Simulator::Now ().GetSeconds ();
		int time_index = static_cast<float> (now/SAMPLING_FREQUENCY) ;

		for (uint32_t rsu_idx = 2 * m_vehicle_num ; rsu_idx < 2 * m_vehicle_num + m_rsu_num; ++rsu_idx)
		{

			double retrans = getAverageRetransmissionTimes (time_index, rsu_idx);
			double path_len = getPathLengthInCoverage (node_idx, rsu_idx);
			double velo = getPreviousVelocity (node_idx);
			// NS_LOG_UNCOND ("velo : " << velo << ", node: " << node_idx);
			if (retrans > 4.5)
				retrans = 4.5;
			double temp =  (path_len / velo)/(5.0/(5.0-retrans));
			cout << "(" << rsu_idx << ", " << retrans << ")"  << endl;
			// cout << "node: " << node_idx <<",rsu_idx:" << rsu_idx << ",retrans:" << retrans << ", path_len: " << path_len << ",velo:" << velo << ",stime:"<<(path_len/velo) <<",score:"<< temp<< endl;
			if (velo == 0 || retrans > m_retrans_lower_bound)
			{
				continue;
			}
			
			
			
			if (max_score < temp)
			{				
				// score is , the bigger the better
				max_score = temp;
				result_rsu_idx = rsu_idx;
				// NS_LOG_UNCOND ("node:" <<node_idx<< "max_score : " << max_score << ",temp:" << temp);
			}
		}
		// cout << "verdict: "<< result_rsu_idx << ", score:" << << endl;
		return result_rsu_idx;
	}
	uint32_t  testNetworkQualitywithScore (uint32_t node_idx)
	{
		// input is the node you have to find a rsu for it
		//output the best rsu

		if( m_my_algorithm_on && type_node (node_idx) == "vehicle")
		{
			uint32_t rsu_idx = getBestRSU (node_idx);
			return rsu_idx;
		}
		else
		{
			// return 0 means no RSU can be connect.
			return getNearestRSUIdx(node_idx);
		}	
	}
	void conn (string context, Mac48Address value)
	{
		uint32_t node_idx = getNodeIndexFromContext (context);
		NS_LOG_UNCOND("assoc distance from rsu : " << getDistance (node_idx, 2 * m_vehicle_num));
	}
	void disconn (string context, Mac48Address value)
	{
		uint32_t node_idx = getNodeIndexFromContext (context);
		NS_LOG_UNCOND("disassoc distance from rsu : " << getDistance (node_idx, 2 * m_vehicle_num));

	}
	void bindVehicleWifiCallback ()
	{
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/Assoc", MakeCallback(&MyTest::connectWifi, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/DeAssoc", MakeCallback(&MyTest::disconnectWifi, this));
	}
	uint32_t getRSUOfVehicle (uint32_t node_idx)
	{
		Ptr<StaWifiMac> vehicle_mac  =  getStaWifiMac (node_idx);
		Ptr<ApWifiMac> rsu_mac ;
		for (uint32_t i = m_vehicle_num * 2; i < m_vehicle_num * 2 + m_rsu_num; ++i)
		{
			rsu_mac  =  getApWifiMac (i);
			if (vehicle_mac->GetBssid () == rsu_mac->GetAddress ())
			{
				return i;
			}
		}
		return 0;
	}

	string getMatchedBssid (Mac48Address addr)
	{
		std::vector<uint32_t> result1;
		ostringstream oss;
		Ptr<StaWifiMac> vehicle_mac;
		
		for (uint32_t i = 0; i < m_vehicle_num; ++i)
		{
			vehicle_mac = getStaWifiMac (i);
			Ptr<Node> vehicle_node = NodeList::GetNode (i);
			Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));	
			if (vehicle_mac->GetBssid () == addr && vehicle_send_wifi_app->isRunning ())
			{
				result1.push_back (i);
				oss << i << ", ";
			}
			
		}
		if (result1.size () > 0)
		{
			double now = Simulator::Now().GetSeconds (); 
			int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) - 1;
			double rx_sum = 0;
			for (int i = 0 ;i < (int)result1.size (); ++i)
			{
				rx_sum += (history[result1[i]+m_vehicle_num].getPktSinkWifiRxBitRates() [time_index]);
			}
			oss << "throughput : " << rx_sum/100.0;
		}
		return oss.str ();
	}
	void watchRSU ()
	{

		for (uint32_t i = m_vehicle_num * 2; i < 2 * m_vehicle_num + m_rsu_num; ++i)
		{		
			string output = getMatchedBssid(getApWifiMac(i)->GetAddress ());
			if (output.size() > 0)
			{
				// NS_LOG_UNCOND ("rsu " << i << " : " << output.c_str ());	
			}
			
		}
		Simulator::Schedule (Seconds (1.0), &MyTest::watchRSU, this);
	}
	void checkConnectionRatio ()
	{
		double now = Simulator::Now ().GetSeconds () ;
		int time_index =  static_cast<int> ((now) / SAMPLING_FREQUENCY) - 1;
		
		vector<uint32_t> pkt_sink_wifi_rx = getPktSinkwifiRxAverage (time_index);
		vector<uint32_t> pkt_sink_lte_rx =  getPktSinkLteRxAverage (time_index) ;

		history[0].LogWifiConnectionNumber (static_cast<double> (pkt_sink_wifi_rx.size ()));
		history[0].LogLteConnectionNumber (static_cast<double> (pkt_sink_lte_rx.size ()));

		Simulator::Schedule (Seconds (0.1), &MyTest::checkConnectionRatio, this);
	}
	 Vector GetPosition (Ptr<Node> node)
	{
	  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
	  return mobility->GetPosition ();
	}
	Vector GetPosition (uint32_t node_idx)
	{
		Ptr<Node> node = NodeList::GetNode (node_idx)	;
		Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
	  	return mobility->GetPosition ();
	}
	double getDistance (uint32_t node_idx_1, uint32_t node_idx_2)
	{
		Vector pos1 = GetPosition (NodeList::GetNode (node_idx_1));
		Vector pos2 = GetPosition (NodeList::GetNode (node_idx_2));

		return sqrt (pow (pos1.x - pos2.x, 2) + pow (pos1.y - pos2.y, 2));
	}
	double getDistance (Vector pos1, Vector pos2)
	{
		return sqrt (pow (pos1.x - pos2.x, 2) + pow (pos1.y - pos2.y, 2));
	}

	void ApTxOkHeaderListener (string context, const WifiMacHeader &header)
	{
		// uint32_t node_idx = getNodeIndexFromContext (context);
		// NS_LOG_UNCOND ("ap id: "<<node_idx << ",header=" << header);
	}
	void ApTxErrHeaderListener (string context, const WifiMacHeader &header)
	{
		// uint32_t node_idx = getNodeIndexFromContext (context);
		// NS_LOG_UNCOND ("ap id: "<<node_idx << ",header=" << header);
	}
	void TxOkHeaderListener (string context, const WifiMacHeader &header)
	{
		// uint32_t node_idx = getNodeIndexFromContext (context);
		// NS_LOG_UNCOND ("id: "<<node_idx << ",header=" << header);
	}
	void TxErrHeaderListener (string context, const WifiMacHeader &header)
	{
		uint32_t node_idx = getNodeIndexFromContext (context);
		Ptr<Node> node= NodeList::GetNode (node_idx);
		Ptr<NetDevice> dev =  node->GetDevice (0);
		Ptr<WifiNetDevice> wifi_dev = dev->GetObject <WifiNetDevice> ();
		Ptr<StaWifiMac> sta_mac = DynamicCast<StaWifiMac> (wifi_dev->GetMac ());
		// uint32_t cw  = sta_mac->GetContentionWindowSize  ();
		// NS_LOG_UNCOND ("cw=" << cw);
		// NS_LOG_UNCOND ("id: "<<node_idx << ",context"<< context<<",header=" << header);
	}
	Ptr<StaWifiMac> getStaWifiMac (uint32_t node_idx)
	{	
		return DynamicCast<StaWifiMac>(getWifiMac (node_idx));	
	}
	Ptr<ApWifiMac> getApWifiMac (uint32_t node_idx)
	{	
		return DynamicCast<ApWifiMac>(getWifiMac (node_idx));	
	}
	Ptr<WifiNetDevice> getWifiNetDevice (uint32_t node_idx)
	{
		Ptr<Node> node= NodeList::GetNode (node_idx);
		Ptr<NetDevice> dev =  node->GetDevice (0);
		return dev->GetObject <WifiNetDevice> ();
	}
	Ptr<WifiMac> getWifiMac (uint32_t node_idx)
	{				
		Ptr<WifiNetDevice> wifi_dev = getWifiNetDevice (node_idx);
		Ptr<WifiMac> wifi_mac = wifi_dev->GetMac ();		
		return wifi_mac;
	}
	Ptr<YansWifiPhy> getWifiPhy (uint32_t node_idx)
	{
		Ptr<Node> node= NodeList::GetNode (node_idx);
		Ptr<NetDevice> dev =  node->GetDevice (0);
		Ptr<WifiNetDevice> wifi_dev = dev->GetObject <WifiNetDevice> ();
		return DynamicCast<YansWifiPhy> (wifi_dev->GetPhy ());
	}

	void checkCW ()
	{
		std::vector<uint32_t> result;
		 double now = Simulator::Now ().GetSeconds () ;
		int time_index =  static_cast<float> ((now) / SAMPLING_FREQUENCY) - 1;
		// NS_LOG_UNCOND ("time : " << now << ", " << time_index );
		for (uint32_t i = 0; i < m_vehicle_num; ++i)
		{
			Ptr<Node> node= NodeList::GetNode (i);
			Ptr<NetDevice> dev =  node->GetDevice (0);
			Ptr<WifiNetDevice> wifi_dev = dev->GetObject <WifiNetDevice> ();
			Ptr<StaWifiMac> sta_mac = DynamicCast<StaWifiMac> (wifi_dev->GetMac ());
			
			uint32_t cw_d  = sta_mac->GetContentionWindowSize  ();

			uint32_t cw_val = static_cast<uint32_t> (log2(cw_d + 1)) - 4;
			m_cw->setCW (time_index, i, cw_val);
			// NS_LOG_UNCOND ("cw=" << cw_d <<",re=" << cw[i]);
		}
		// NS_LOG_UNCOND ("avg cw = " << average(result));
		// for (uint32_t i = 0; i < m_vehicle_num; ++i)
		// {
		// 	NS_LOG_UNCOND("cw " << i <<" : "<<  m_cw->getCW (time_index, i) );
		// }
		Simulator::Schedule (Seconds (0.1), &MyTest::checkCW, this);
	}
	double getWeightedCW (int time_index, uint32_t node_idx)
	{
		int range = 50;
		double result = 0;
		double portion = 0.8;
		std::vector<double> debug_v;
		std::vector<double> debug_cw;
		if (time_index < range )
		{
			// NS_LOG_UNCOND ("unweighted");
			return m_cw->getCW (time_index, node_idx);
		}

		for (int i = 0; i < range; ++i)
		{
			if (i < range/2 )
			{
				double temp = (m_cw->getCW (time_index-i, node_idx) * ( portion/( range/2.)));
				debug_cw.push_back (m_cw->getCW (time_index-i, node_idx));
				debug_v.push_back (temp);
				result += temp;
			}
			else
			{	
				double temp = (m_cw->getCW (time_index-i, node_idx) * ( ( 1 - portion)/( range/2.)));
				debug_cw.push_back (m_cw->getCW (time_index-i, node_idx));
				debug_v.push_back (temp);
				result += temp;
			}
		}
		// cout << "CW composition : ";
		// for (int i = 0; i < static_cast <int> (debug_cw.size ()); i++)
		// {
		// 	cout << "(" << debug_v[i] << ", "<<debug_cw[i] <<")";
		// }
		
		// cout << ",vec_sum" << result << ")";
		// cout << endl;
		return result;
	}
	double getAverageRetransmissionTimes(int time_index)
	{
		std::vector<double> result;
		for (uint32_t i = 0; i < m_vehicle_num; ++i)
		{
			double  current_retrans = getWeightedCW (time_index, i);
			if(current_retrans > 0 && getDistance (i, 2 * m_vehicle_num) < m_coverage_threshold)
			{
				result.push_back (current_retrans);
			}
		}
		return average(result);
	}
	double getAverageRetransmissionTimes(int time_index, uint32_t rsu_idx)
	{
		
		std::vector<double> result;
		if (m_rsu_status->getRetransTimes (time_index, rsu_idx) != -1)
		{
			// NS_LOG_UNCOND ("from array ("<<time_index<<", "<<rsu_idx<<")");		
			return m_rsu_status->getRetransTimes (time_index, rsu_idx);
		}

		for (uint32_t i = 0; i < m_vehicle_num; ++i)
		{
			Ptr<StaWifiMac> sta_mac = getStaWifiMac (i);
			Ptr<ApWifiMac> ap_mac = getApWifiMac (rsu_idx);
			// NS_LOG_UNCOND("bssid mac: " <<  sta_mac->GetBssid ());
			// NS_LOG_UNCOND("mac: " <<  ap_mac->GetAddress ());
			bool is_connect =  sta_mac->GetBssid () == ap_mac->GetAddress ();
			if (is_connect)
			{
				result.push_back (getWeightedCW (time_index, i));
			}
		}
		// NS_LOG_UNCOND ("from calculate ("<<time_index<<", "<<rsu_idx<<")");		
		m_rsu_status->setRetransTimes (time_index, rsu_idx, average (result));
		return m_rsu_status->getRetransTimes (time_index,  rsu_idx);
	}

	void MacTxDataFailListener (string context, Mac48Address value)
	{
		// NS_LOG_UNCOND ("mac tx fail, " << context);
		// uint32_t node_idx = getNodeIndexFromContext (context);
		// double now = Simulator::Now ().GetSeconds ();
		// int time_index = static_cast<int> ((now));
		// if (getDistance (node_idx, 2 * m_vehicle_num) < m_coverage_threshold)
		// {
		// 	cw[node_idx] += 1;
		// }
		// NS_LOG_UNCOND (node_idx << " re:" <<  ++m_cw.cw[time_index][node_idx]);

	}
	
	void MacTxFinalDataFailListener (string context, Mac48Address value)
	{
		// NS_LOG_UNCOND ("mac tx fail, " << context);
		// uint32_t node_idx = getNodeIndexFromContext (context);
		// double now = Simulator::Now ().GetSeconds ();
		// int time_index = static_cast<int> ((now));
		// if (getDistance (node_idx, 2 * m_vehicle_num) < m_coverage_threshold)
		// {
		// 	cw[node_idx] = 0;
		// }
		// NS_LOG_UNCOND (node_idx << " re:" <<  ++m_cw.cw[time_index][node_idx]);

	}
	void testSetSsid ()
	{

		Ptr<Node> node= NodeList::GetNode (0);
		Ptr<NetDevice> dev =  node->GetDevice (0);
		Ptr<WifiNetDevice> wifi_dev = dev->GetObject <WifiNetDevice> ();
		Ptr<StaWifiMac> sta_mac = DynamicCast<StaWifiMac> (wifi_dev->GetMac ());
		sta_mac->SetSsid (Ssid ("wifi2"));
		Simulator::Schedule (Seconds (10.0), &MyTest::testSetSsid, this);			
	}
	double getTimetoRSU (uint32_t node_idx, uint32_t rsu_idx)
	{
		double dist = getDistance (node_idx, rsu_idx) - m_coverage_threshold
;
		return dist / 14.0;
	}
	uint32_t getNearestRSUIdx (uint32_t node_idx)
	{
		uint32_t nearest_rsu_idx =  0;
		double nearest_rsu_dist = 1000000.0;
		Ptr<Node> vehicle_node = NodeList::GetNode (node_idx);
		Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));	
		if(vehicle_send_wifi_app->isRunning ())
		{
			return 0;
		}

		for (uint32_t rsu_idx = 2 * m_vehicle_num; rsu_idx < 2 * m_vehicle_num + m_rsu_num; ++rsu_idx)
		{
			// NS_LOG_UNCOND ("rsu : " << rsu_idx - 2*m_vehicle_num << ",path length : " << getPathLengthInCoverage (node_idx, rsu_idx));
			double dist = getDistance (node_idx, rsu_idx);
			// NS_LOG_UNCOND ("the node "<<node_idx<<" < 300, it distance = "<< dist << ", it pos = "<<GetPosition (NodeList::GetNode (node_idx)));
			// if (dist < m_coverage_threshold)
			// {
			// 	connectWifiActive (node_idx);
			// 	// NS_LOG_UNCOND ("in :" << rsu_idx);
				
			// }
			// else
			// {				
			// 	disconnectWifiActive (node_idx);
			// 	// NS_LOG_UNCOND ("out of :" << rsu_idx);
			// }
			// if in the coverage, than update nearest rsu target
			if (dist < nearest_rsu_dist)
			{
				nearest_rsu_dist = dist;
				nearest_rsu_idx = rsu_idx;
			}			
		}
		return nearest_rsu_idx;

	}

	void checkVehicleDistance (uint32_t node_idx)
	{ 
		// find the nearest rsu exist or not
		uint32_t good_rsu = testNetworkQualitywithScore (node_idx);
		

		// NS_LOG_UNCOND (node_idx << " checkVehicleDistance : " << (sta_mac) << ", distance to "<< nearest_rsu_idx<<" : " << getDistance (node_idx, nearest_rsu_idx));
		
		// NS_LOG_UNCOND ( node_idx << " check vehicle distance , nearest rsu :" << (nearest_rsu_idx - 2 * m_vehicle_num));
		Ptr<StaWifiMac> sta_mac = getStaWifiMac (node_idx);
		Ptr<Node> vehicle_node = NodeList::GetNode (node_idx);
		Ptr<MyApp> vehicle_send_wifi_app = DynamicCast<MyApp> (vehicle_node->GetApplication (0));	
		if ( good_rsu > 0 && getDistance (node_idx, good_rsu) < m_coverage_threshold)
		{
			NS_LOG_UNCOND (node_idx << " :rsu exists : distnce " << getDistance (node_idx, good_rsu) << ", nearest rsu:" << (good_rsu - 2 * m_vehicle_num));	

			connectWifiActive (node_idx, good_rsu);		
		}

		if (0 != getRSUOfVehicle (node_idx)&& (vehicle_send_wifi_app->isRunning () && getDistance (node_idx, getRSUOfVehicle (node_idx)) > m_coverage_threshold))
		{	
			// NS_LOG_UNCOND ( node_idx << "do disconnect, disconnect from " << getRSUOfVehicle (node_idx));
			disconnectWifiActive (node_idx);
		}

		Simulator::Schedule (Seconds (1.0), &MyTest::checkVehicleDistance, this,  node_idx);	
		// double sampling_f = getTimetoRSU (node_idx, nearest_rsu_idx);
		// NS_LOG_UNCOND (node_idx<<"next time" << sampling_f);
		// if (nearest_rsu_idx != 0 && sampling_f > 1.0)
		// {
			
		// }
		

		

		
	}
	void setVehicleStatus ()
	{
		for (uint32_t i = 0; i < m_vehicle_num; ++i)
		{
			Vector old_position = m_vehicle_status->getPosition (i);
			Vector new_position = Vector (GetPosition (i));
			// Vector diff_position = Vector(new_position.x - old_position.x,
			// 							new_position.y - old_position.y,
			// 							new_position.z - old_position.z);
			// NS_LOG_UNCOND ("old  : " << old_position);
			// NS_LOG_UNCOND ("new  : " << new_position);

			// NS_LOG_UNCOND ("diff : " << diff_position);
			double new_velo = pow (pow(new_position.x - old_position.x, 2) + pow(new_position.y - old_position.y, 2) + pow(new_position.z - old_position.z, 2) , 1/2.0)/0.5;

			m_vehicle_status->setPosition (i, new_position);
			// NS_LOG_UNCOND ("set velo : " << new_velo << ", node: " << i);
			m_vehicle_status->setVelocity (i, new_velo);
		}
		Simulator::Schedule (Seconds (0.5), &MyTest::setVehicleStatus, this);
	}

	void checkVehicleAttachWhichRSU()
	{
		
		for (uint32_t i = 0; i < m_vehicle_num; ++i)
		{
			Ptr<UniformRandomVariable> waiting_time = CreateObject<UniformRandomVariable> ();
			waiting_time->SetAttribute ("Min", DoubleValue (0.01));
			waiting_time->SetAttribute ("Max", DoubleValue (0.1));
			Simulator::Schedule (Seconds (waiting_time->GetValue ()), &MyTest::checkVehicleDistance, this,  i);	
		}
	}
	void turnInactive (string context, Mac48Address value)
	{

		
		string path = context.substr(0, context.find ("/Assoc"));
		path += "/ActiveProbing";
		NS_LOG_UNCOND ("turnInactive" <<path.c_str ());

		Config::Set (path.c_str (), BooleanValue (false));

	}
	void setWifiWake (uint32_t node_idx)
	{
		Ptr<YansWifiPhy> wifiphy = getWifiPhy (node_idx);
		wifiphy->ResumeFromSleep ();		
	}
	void setWifiSleep (uint32_t node_idx)
	{
		Ptr<YansWifiPhy> wifiphy = getWifiPhy (node_idx);
		wifiphy->SetSleepMode ();
		
	}
	void setAllVehicleWifiSleep ()
	{
		for (uint32_t i = 0; i < m_vehicle_num; ++i)
		{
			setWifiSleep (i);
		}
	}
	void Ipv4DropListener (string context, const Ipv4Header &header, Ptr< const Packet > packet, Ipv4L3Protocol::DropReason reason, Ptr< Ipv4 > ipv4, uint32_t interface)
	{
		NS_LOG_UNCOND ("ipv4 packet drop" << reason << context);

	}
	void P2PMacRxDropListener (string context, Ptr<const Packet> pkt)
	{
		NS_LOG_UNCOND (context << pkt);
	}
	void P2PMacTxDropListener (string context, Ptr<Packet> pkt)
	{
		NS_LOG_UNCOND (context<< pkt);	
	}
	void RSUThroughputListener (string context, Ptr<const Packet> pkt)
	{
		// NS_LOG_UNCOND ("rsu rx " << "," << context << endl<<*pkt);
		uint32_t node_idx = getNodeIndexFromContext (context);
		history[node_idx].IncRSURx (pkt->GetSize ());
		
	}
	void RSUCountListener ()
	{
		for (std::vector<double>::iterator it = m_rsu_connection_count.begin(); it != m_rsu_connection_count.end(); ++it)
		{
			int rsu_idx =  (it - m_rsu_connection_count.begin ()) + m_vehicle_num * 2;
			history[rsu_idx].SetRSUConnectionCount (*it);
		}
		Simulator::Schedule (Seconds (0.1), &MyTest::RSUCountListener, this);
	}
	virtual void bindCallback ()
	{
		// #callback
		// bind to the same listener with macRx macTx
		// Simulator::Schedule (Seconds (10.0), &MyTest::testSetSsid, this);
		Simulator::Schedule (Seconds (0.1), &MyTest::checkCW, this);
		Simulator::Schedule (Seconds (1), &MyTest::checkConnectionRatio, this);
		Simulator::Schedule (Seconds (0.1), &MyTest::setVehicleStatus, this);
		Simulator::Schedule (Seconds (1.1), &MyTest::checkVehicleAttachWhichRSU, this);
		Simulator::Schedule (Seconds (1.), &MyTest::setAllVehicleWifiSleep, this);
		Simulator::Schedule (Seconds (1), &MyTest::RSUCountListener, this);

		if (m_is_debug)
		{
			Simulator::Schedule (Seconds (1.0), &MyTest::watchRSU, this);
		}
		
		
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::ApWifiMac/MacTxDrop", MakeCallback(&MyTest::MacTxDropListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::ApWifiMac/MacRxDrop", MakeCallback(&MyTest::MacRxDropListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::ApWifiMac/MacTx", MakeCallback(&MyTest::MacTxListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::ApWifiMac/MacRx", MakeCallback(&MyTest::MacRxListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/$ns3::YansWifiPhy/PhyTxDrop" , MakeCallback(&MyTest::PhyTxDropListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/$ns3::YansWifiPhy/PhyRxDrop" , MakeCallback(&MyTest::PhyRxDropListener, this));
		Config::Connect("/NodeList/*/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback (&MyTest::PktSinkRxListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/MacRxDrop", MakeCallback(&MyTest::MacRxDropListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/MacTxDrop", MakeCallback(&MyTest::MacTxDropListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/MacRx", MakeCallback(&MyTest::MacRxListener, this));
		Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/MacTx", MakeCallback(&MyTest::MacTxListener, this));
		
		Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/RemoteStationManager/MacTxDataFailed", MakeCallback(&MyTest::MacTxDataFailListener, this));
		Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/RemoteStationManager/MacTxFinalDataFailed", MakeCallback(&MyTest::MacTxFinalDataFailListener, this));


		for (uint32_t i = m_vehicle_num * 2; i < m_vehicle_num * 2 + m_rsu_num; ++i)
		{
			ostringstream oss;
			oss << "/NodeList/" << i << "/DeviceList/2/$ns3::CsmaNetDevice/TxQueue/Enqueue";
			Config::Connect (oss.str ().c_str (), MakeCallback(&MyTest::RSUThroughputListener, this));
		}



		

		Simulator::Schedule (Seconds(0.), &MyTest::checkTime, this);

		// bindVehicleWifiCallback ();
	}
	void setupLogFilename ()
	{
		if (m_my_algorithm_on)
		{
			for (std::vector<NodeHistory>::iterator it = history.begin(); it != history.end(); ++it)
			{
				ostringstream oss;
				oss << "with_algo_th" << m_retrans_threshold << "_";
				it->setupFilename (oss.str ().c_str ());
			}
				
		}

		
	}
	static void PcapMacTx (Ptr<PcapFileWrapper> file, Ptr<const Packet> p)
	{
		  // NS_LOG_UNCOND (" PcapMacTx at " << Simulator::Now ().GetSeconds ());
		  file->Write (Simulator::Now (), p);
	}
	static void PcapMacRx (Ptr<PcapFileWrapper> file, Ptr<const Packet> p)
	{
		  // NS_LOG_UNCOND ("PcapMacRx at " << Simulator::Now ().GetSeconds ());
		  file->Write (Simulator::Now (), p);
	}
	void setupPcapListener ()
	{		
  		for (uint32_t i = 0; i <  m_vehicle_num; ++i)
  		{
  			PcapHelper pcapHelper;
  			ostringstream oss;
  			oss << "main-vehicle-"<<i<<".pcap";
  			Ptr<PcapFileWrapper> file = pcapHelper.CreateFile (oss.str ().c_str (), std::ios::out, PcapHelper::DLT_PPP);
  			Ptr<StaWifiMac> mac = getStaWifiMac (i);
  			mac->TraceConnectWithoutContext ("MacTx", MakeBoundCallback (&(MyTest::PcapMacTx), file));
  			mac->TraceConnectWithoutContext ("MacRx", MakeBoundCallback (&(MyTest::PcapMacRx), file));
  		}
  	// 	for (uint32_t i = m_vehicle_num; i < 2 * m_vehicle_num; ++i)
  	// 	{
  	// 		PcapHelper pcapHelper;
  	// 		ostringstream oss;
  	// 		oss << "main-remote-"<<i<<".pcap";
  	// 		Ptr<PcapFileWrapper> file = pcapHelper.CreateFile (oss.str ().c_str (), std::ios::out, PcapHelper::DLT_PPP);
  	// 		// Ptr<WifiNetDevice> wifi_net = getWifiNetDevice (i); 
  	// 		Ptr<Node> node= NodeList::GetNode (i);
			// Ptr<CsmaNetDevice> dev =  DynamicCast<CsmaNetDevice>(node->GetDevice (0));			
			

  	// 		dev->TraceConnectWithoutContext ("MacTx", MakeBoundCallback (&(MyTest::PcapMacTx), file));
  	// 		dev->TraceConnectWithoutContext ("MacRx", MakeBoundCallback (&(MyTest::PcapMacRx), file));
  	// 	}
	}
	virtual void go (int argc, char* argv[])
	{
		parseCmd (argc, argv);
		deployEnvironment ();

		
		m_node_num = NodeList::GetNNodes ();
		history = vector<NodeHistory> (m_node_num);
		bindCallback ();
		setupPcapListener ();
		setupLogFilename ();
		AnimationInterface anim ("animation.xml");
		runSimulator ();
		if (m_is_logging)
		{

			NodeHistory::dumpAllNodes(history, m_stop_time, m_vehicle_num);	
		}
		

	}

	void runSimulator ()
	{

		Simulator::Stop (Seconds(m_stop_time));
		Simulator::Run ();
		Simulator::Destroy ();
	}
	virtual NodeFactory* createHelper (string helper)

	{
		if (helper == "vehicle")
		{
			//prefetch the number of remote nodes number to avoid setup too much sink ip
			m_node_group[0]->m_remote_nodes_num = m_node_group[1]->num;
			m_node_group[0]->m_speed_min = m_vehicle_speed_min;
			m_node_group[0]->m_speed_max = m_vehicle_speed_max;
			m_node_group[0]->m_bound = m_vehicle_bound;
			m_node_group[0]->m_bg_traffic = m_vehicle_background_traffic;
			m_node_group[0]->num = m_vehicle_num;
			m_node_group[0]->m_mobility_mode = m_mobility_mode;
			m_node_group[0]->m_send_rate = m_send_rate;

			return new VehicleFactory (m_node_group[0]);
		}
		else if (helper == "remote host")
		{
			m_node_group[1]->num = m_vehicle_num;
			m_node_group[1]->m_wifi = m_node_group[0]->m_wifi;
			m_node_group[1]->m_wifiMac = m_node_group[0]->m_wifiMac;
			m_node_group[1]->m_wifiPhy = m_node_group[0]->m_wifiPhy;
			m_node_group[1]->m_ipv4 = m_node_group[0]->m_ipv4;
			return new RemoteHostFactory (m_node_group[1]);
		}
		else if (helper == "rsu")
		{	
			m_node_group[2]->remote_nodes = m_node_group[1]->remote_nodes;
			m_node_group[2]->m_wifi = m_node_group[1]->m_wifi;
			m_node_group[2]->m_wifiMac = m_node_group[1]->m_wifiMac;
			m_node_group[2]->m_wifiPhy = m_node_group[1]->m_wifiPhy;
			m_node_group[2]->m_ipv4 = m_node_group[1]->m_ipv4;
			m_node_group[2]->num = m_rsu_num;
			return new RSUFactory (m_node_group[2]);
		}
		else if (helper == "enodeb")
		{
			//get the vehicle nodeinstance from vehicle factory
			m_node_group[3]->vehicle_nodes = m_node_group[0]->vehicle_nodes;
			m_node_group[3]->remote_nodes = m_node_group[1]->remote_nodes;
			return new ENodeBFactory (m_node_group[3]);
		}
		else if (helper == "pgw")
		{
			m_node_group[4]->vehicle_nodes = m_node_group[0]->vehicle_nodes;
			m_node_group[4]->m_vehicle_lte_dev = m_node_group[3]->m_vehicle_lte_dev;
			m_node_group[4]->m_enodeb_dev = m_node_group[3]->m_enodeb_dev;
			m_node_group[4]->lte_helper = m_node_group[3]->lte_helper;
			m_node_group[4]->epc_helper= m_node_group[3]->epc_helper;
			m_node_group[4]->remote_nodes = m_node_group[1]->remote_nodes;
			m_node_group[4]->m_remote_dev = m_node_group[3]->m_remote_dev;
			m_node_group[4]->pgw_node = m_node_group[3]->pgw_node;
			m_node_group[4]->m_send_rate = m_send_rate;
			return new PgwFactory (m_node_group[4]);
		}
		else if (helper == "probe node")
		{
		
			m_node_group[5]->m_wifi = m_node_group[1]->m_wifi;
			m_node_group[5]->m_wifiMac = m_node_group[1]->m_wifiMac;
			m_node_group[5]->m_wifiPhy = m_node_group[1]->m_wifiPhy;
			m_node_group[5]->m_ipv4 = m_node_group[2]->m_ipv4;
			m_node_group[5]->m_rsu_nodes = m_node_group[2]->m_rsu_nodes;

			return new ProbeNodeFactory (m_node_group[5]);
		}
		else
		{
			NS_ASSERT_MSG (false, "create Helper is out of control.");
			return NULL;

		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///    node container index order : 0 -> vehicle -> remote host -> rsu -> enodeb ->  pgw -> probe node     ////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	virtual uint32_t getLocalIndex (uint32_t global_idx)
	{
		uint32_t result = global_idx;
		uint32_t check_error = 0;
		for (vector<SharedPreference*>::iterator it=m_node_group.begin (); it != m_node_group.end (); ++it)
		{
			check_error += (*it)->num;
			if (result < (*it)->num)
			{
				break;
			}
			result -= (*it)->num;
			
		}
		NS_ASSERT_MSG (global_idx < check_error, "parameter of getLocalIndex is out of bound");
		return result;
	}
	virtual uint32_t getGlobalIndex (uint32_t idx, string type)
	{
		uint32_t temp_sum = 0;
		for (vector<SharedPreference*>::iterator it = m_node_group.begin (); it != m_node_group.end (); ++it)
		{
			if ((*it)->name != type)
			{
				temp_sum += (*it)->num;
			}
			else
			{
				NS_ASSERT_MSG ((*it)->num > idx, "during get the global index, invalid " + (*it)->name);
				break;
			}
		}
		return temp_sum + idx ;

	}
	virtual string type_node (uint32_t global_idx)
	{
		string result = "";
		for (vector<SharedPreference*>::iterator it = m_node_group.begin(); it != m_node_group.end(); ++it)
		{
			if (global_idx < (*it)->num)
			{
				result = (*it)->name;
				break;
			}
			global_idx -= (*it)->num;	
		}
		return result;
	}
	
	


	
private:
	uint32_t m_rsu_num ;
	class ContentionWindowLog {
		public:

			std::vector<std::vector<uint32_t> > slots;
			ContentionWindowLog ()
			{

			}
			ContentionWindowLog (uint32_t time, uint32_t node)
			{
				NS_LOG_UNCOND ("debug : time num=" << time << ", node num=" << node);
				slots = vector<vector<uint32_t> >(time,vector<uint32_t> (node, 0));
			}
			void setCW(int time, uint32_t node, uint32_t cw_val)
			{
				slots[time][node] = cw_val;
			}
			uint32_t getCW(int time, uint32_t node)
			{

				return slots[time][static_cast<int>(node)];
			}			
	
	};
	double m_coverage_threshold;
	ContentionWindowLog* m_cw;
	std::vector<uint32_t> cw;
	bool m_is_debug;
	NodeContainer m_nodes_list;
	double m_simulator_stop_time;
	uint32_t m_node_num;
	uint32_t m_vehicle_num;
	vector<SharedPreference*> m_node_group;

	double m_stop_time;
	bool m_is_logging;
	vector<NodeHistory> history;
	double start_time;
	double m_simulation_duration;
	double m_sampling_frequency;
	bool m_my_algorithm_on;
	double m_retrans_threshold;
	VehicleStatus* m_vehicle_status;
	double m_retrans_lower_bound;
	RSUStatus* m_rsu_status;
	double m_vehicle_speed_min;
	double m_vehicle_speed_max;
	double m_vehicle_bound;
	bool m_vehicle_background_traffic;
	string m_log_filename;
	vector<double> m_rsu_connection_count;
	string m_mobility_mode;
	string m_send_rate;

};

std::vector<string > readBatchFile (string path)
{
	string dir = path;
	string batch_file = "batch.cfg";

	ifstream fin (string(dir + batch_file).c_str ());
	std::vector<string > configs_list;
	NS_LOG_UNCOND ("Batch List");
	while (fin)
	{	
		string buf;
		fin >> buf;	
		if (buf.size () < 1 || buf[0]=='#')
		{
			continue;
		}
		configs_list.push_back (dir + buf);
		NS_LOG_UNCOND (dir + buf);
	}

	return configs_list;
}
void setupLogDirectory ()
{
	string root_path ("log/");
	string path_dir (NodeHistory::mygetdate ()+string ("/"));
	
	
	makeLogDir (root_path + path_dir);
	NS_LOG_UNCOND ("setupLogDirectory/debug : " << root_path + path_dir);
	ostringstream oss;
	oss << int(getLastDirNameInt(root_path + path_dir)+1) << "/";
	makeLogDir (root_path + path_dir+  oss.str ());
	NS_LOG_UNCOND ("setupLogDirectory/debug : " << root_path + path_dir+ oss.str ());
}
int main(int argc, char* argv[])
{
	double enbTxPowerDbm = 46.0;
	Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (enbTxPowerDbm));
	
	std::vector<string> experiments_time_log;
	std::vector<string> configs_path_list;
	configs_path_list = readBatchFile ("CFG/");
	setupLogDirectory ();
	cout << "file counts " << configs_path_list.size () << endl;

	for (std::vector<string>::iterator it = configs_path_list.begin(); it != configs_path_list.end(); ++it)
	{
		MyTest test ;
		test.readConfig (*it);
		experiments_time_log.push_back (NodeHistory::gettime ());
		cout << "start " << (it- configs_path_list.begin ())<< ":" << NodeHistory::gettime () << endl;
		test.go (argc, argv);
	}
	
	for (std::vector<string>::iterator it = experiments_time_log.begin(); it != experiments_time_log.end(); ++it)
	{
		cout << it - experiments_time_log.begin () << "@" << *it << endl;
	}
	NS_LOG_UNCOND ("DONE!!!!!!!!!!!!!!!!!!!!!!!!");

	
	return 0;	


}


