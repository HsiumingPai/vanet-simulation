#ifndef NODE_FACTORY_H
#define NODE_FACTORY_H
#include "my-macro.h"
#include "ns3/csma-module.h"
#include "ns3/bridge-helper.h"

NS_LOG_COMPONENT_DEFINE ("my-node-factory");

	class NodeFactory
	{

	public:
		//  "=  0" is pure virtual function, just a C++ syntax.
		virtual void SetupMobility () = 0;
		virtual void SetupIP () = 0;
		virtual void SetupNetDev () = 0;
		virtual void SetupApp () = 0;
	};

	class VehicleFactory : public NodeFactory
	{
		// #vehicle
		NodeContainer m_nodes;
		SharedPreference* m_setting;
		double min_bound;
		double max_bound;
	public:
		VehicleFactory (const VehicleFactory& obj)
		{
			m_nodes = obj.m_nodes;
			m_setting = obj.m_setting;
		}
		VehicleFactory ()
		{		

		}
		VehicleFactory (SharedPreference* setting) 
		{

			m_setting = setting;
			m_nodes.Create (setting->num);
			m_setting->vehicle_nodes = m_nodes;
			NS_LOG_DEBUG("create : " << setting->num << endl);
			min_bound = -1 * m_setting->m_bound;
			max_bound = m_setting->m_bound;
		}
		void setupGridPosition (MobilityHelper& m)
		{
			double y_volume = 50;
			double x_volume = 250;
			uint16_t grid_width = 5;
			m.SetPositionAllocator ("ns3::GridPositionAllocator",
			"MinX", DoubleValue (-1.0 * m_setting->m_bound),
			"MinY", DoubleValue (-1. * y_volume / 2.0),
			"DeltaX", DoubleValue ( (x_volume/static_cast<double>(grid_width-1))),
			"DeltaY", DoubleValue (y_volume / static_cast<double>(grid_width-1)),
			"GridWidth", UintegerValue (grid_width),
			"LayoutType", StringValue ("ColumnFirst"));
		}
		void setupRandomDiskPosition (MobilityHelper& m)
		{
			m.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                	           "X", StringValue ("-550.0"),
                   	           "Y", StringValue ("0.0"),
                               "Rho", StringValue ("ns3::UniformRandomVariable[Min=5|Max=125]"));
 
		}
		virtual void SetupMobility ()
		{
			// NS_LOG_DEBUG(" setup mobility " << endl);
			MobilityHelper mobility;
			
			// "MinX", DoubleValue (-1 * base * 2),
			// "MinY", DoubleValue (-1 * base * mul),
			string mobility_model = m_setting->m_mobility_mode;
			if (mobility_model ==string("grid"))
			{
				setupGridPosition (mobility);
			}
			else 
			{
				setupRandomDiskPosition (mobility);
			}
			

			ostringstream bound_oss;
			bound_oss << min_bound << "|" << max_bound << "|" << min_bound << "|" << max_bound ;

			ostringstream oss;
			oss << STOP_TIME;
			ostringstream speed_oss;
			speed_oss<< "ns3::UniformRandomVariable[Min=";
			speed_oss<<m_setting->m_speed_min;
			speed_oss<< "|Max="<<m_setting->m_speed_max<<"]";	
			mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",	
				"Mode", StringValue ("Time"),
				"Time", StringValue(oss.str ().c_str ()),
				"Speed", StringValue (speed_oss.str ().c_str ()),
				"Bounds", StringValue (bound_oss.str ().c_str ()),
				"Direction", StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));
			//Bounds  is equal to rectangle (x_min, x_max, y_min, y_max)
			// mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install (m_nodes);
		}
		virtual void SetupNetDev ()
		{	
			NS_LOG_DEBUG(" setup net dev " << endl);
			m_setting->m_wifi = getWifiHelper ();
			m_setting->m_wifiMac = getWifiMac ();
			m_setting->m_wifiPhy = getWifiPhy ();
			m_setting->m_dev = m_setting->m_wifi.Install (m_setting->m_wifiPhy , m_setting->m_wifiMac, m_nodes);
			// Ptr<WifiNetDevice> wifi_dev = DynamicCast<WifiNetDevice> (m_setting->m_dev.Get (4));
			// wifi_dev->GetPhy ()->SetChannelNumber (3);
		}
		virtual void SetupIP ()
		{
			NS_LOG_DEBUG(" setup ip " << endl);	
			InternetStackHelper	inet;
			inet.Install (m_nodes);
			m_setting->m_ipv4.SetBase ("192.168.1.0",  "255.255.255.0");
			m_setting->m_ipv4.Assign (m_setting->m_dev);
		}

		virtual void SetupApp ()
		{
			// setupRecvApp ();
			setupSendApp ();

		}

		void setupRecvApp ()
		{
			NS_LOG_DEBUG(" setup app, recv from remote " << endl);
			uint16_t sink_port = 6000;
			for (uint32_t i = 0; i < m_nodes.GetN (); ++i)
			{
				/* code */
				Address remote_addr (InetSocketAddress (Ipv4Address::GetAny (), sink_port));
				PacketSinkHelper pkt_sinker ("ns3::UdpSocketFactory", remote_addr);
				
				ApplicationContainer apps = pkt_sinker.Install (m_nodes.Get (i));
				apps.Start (Seconds (0.0));
				apps.Stop (Seconds (STOP_TIME));
			}

		}
		void setupSendApp ()
		{
			NS_LOG_DEBUG(" setup app, send to remote" << endl);
			uint16_t sink_port = 8000;
			uint32_t sub_ip = m_nodes.GetN () + 1;	
			for (uint32_t i = 0; i < m_nodes.GetN (); ++i)
			{
				Ptr<Socket> sock = Socket::CreateSocket (m_nodes.Get (i), UdpSocketFactory::GetTypeId ());
				Ptr<MyApp> app = CreateObject<MyApp> ();
				ostringstream sink_ip ;
				sink_ip << "192.168.1." <<  sub_ip ++;
				// NS_LOG_UNCOND("sink ip "<< i << " :'" << sink_ip.str ().c_str () << endl);
				app->Setup (sock, InetSocketAddress (sink_ip.str ().c_str (), sink_port), m_setting->m_payload, m_setting->m_pkts_num, DataRate (m_setting->m_send_rate.c_str ()));
				m_nodes.Get (i)->AddApplication (app);
				app->SetStartTime (Seconds (1.0));
				app->SetStopTime (Seconds (STOP_TIME));
			}

		}
		static YansWifiPhyHelper getWifiPhy ()
		{
			YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
			double tx_power = 27.49;
			wifiPhy.Set ("TxPowerStart", DoubleValue(tx_power));
			wifiPhy.Set ("TxPowerEnd", DoubleValue(tx_power));
			YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
			wifiPhy.SetChannel (wifiChannel.Create ());
			return wifiPhy;
		}


		static NqosWifiMacHelper getWifiMac ()
		{	
			NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
			wifiMac.SetType ("ns3::StaWifiMac",
			"Ssid", SsidValue (Ssid ("wifi0")),
			"ActiveProbing", BooleanValue (true));
			return wifiMac;
		}
		static WifiHelper getWifiHelper ()
		{
			WifiHelper wifi = WifiHelper::Default ();
			wifi.SetStandard (WIFI_PHY_STANDARD_80211a);
			// wifi.SetRemoteStationManager  ("ns3::AarfWifiManager");
			wifi.SetRemoteStationManager ("ns3::ArfWifiManager");
			// wifi.SetRemoteStationManager  ("ns3::ConstantRateWifiManager",
			// 				"DataMode", StringValue ("OfdmRate6Mbps"));
			return wifi;
		}
	};

	class RemoteHostFactory : public NodeFactory
	{
		//#remote host
		NodeContainer m_nodes;
		SharedPreference* m_setting;
	public:

		RemoteHostFactory (SharedPreference* setting) 
		{
			m_setting = setting;
			m_nodes.Create (setting->num);
			m_setting->remote_nodes = m_nodes;
			NS_LOG_DEBUG("create : " << setting->num << endl);
		}

		virtual void SetupApp ()
		{
			setupRecvApp ();
			// setupSendApp ();
		}
		void setupSendApp ()
		{
			NS_LOG_DEBUG(" setup app, send to vehicle " << endl);
			uint16_t sink_port = 6000;
			uint32_t sub_ip = 1;
			for (uint32_t i = 0; i < m_nodes.GetN (); ++i)
			{
				Ptr<Socket> sock = Socket::CreateSocket (m_nodes.Get (i), UdpSocketFactory::GetTypeId ());
				Ptr<MyApp> app = CreateObject<MyApp> ();
				ostringstream sink_ip ;
				sink_ip << "192.168.1." << sub_ip++;
				NS_LOG_DEBUG("sink ip "<< i << " :'" << sink_ip.str ().c_str () << endl);
				app->Setup (sock, InetSocketAddress (sink_ip.str ().c_str (), sink_port), m_setting->m_payload, m_setting->m_pkts_num, DataRate (SEND_RATE));
				m_nodes.Get (i)->AddApplication (app);
				app->SetStartTime (Seconds (1.0));
				app->SetStopTime (Seconds (STOP_TIME));
			}

		}
		void setupRecvApp ()
		{

			NS_LOG_DEBUG(" setup app , recv from vehicle" << endl);
			uint16_t sink_port = 8000;
			for (uint32_t i = 0; i < m_nodes.GetN (); ++i)
			{
				/* code */
				Address remote_addr (InetSocketAddress (Ipv4Address::GetAny (), sink_port));
				PacketSinkHelper pkt_sinker ("ns3::UdpSocketFactory", remote_addr);
				
				ApplicationContainer apps = pkt_sinker.Install (m_nodes.Get (i));
				apps.Start (Seconds (0.0));
				apps.Stop (Seconds (STOP_TIME));
			}

		}
		virtual void SetupMobility ()
		{
			NS_LOG_DEBUG(" setup mobility " << endl);
			MobilityHelper mobility;
			mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
			"MinX", DoubleValue (30.0),
			"MinY", DoubleValue (150.0), 
			"DeltaX", DoubleValue (40.0),
			"DeltaY", DoubleValue (40.0),
			"GridWidth", UintegerValue (4),
			"LayoutType", StringValue ("RowFirst"));
			mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install (m_nodes);
		}
		virtual void  SetupNetDev ()
		{
			NS_LOG_DEBUG(" setup Net Dev " << endl);	
			// m_setting->m_dev = m_setting->m_wifi.Install (m_setting->m_wifiPhy , m_setting->m_wifiMac, m_nodes);		

		}
		virtual void SetupIP ()
		{
			NS_LOG_DEBUG(" setup IP " << endl);
			InternetStackHelper inet;
			inet.Install (m_nodes);
			// m_setting->m_ipv4.Assign (m_setting->m_dev);
		}
		
	};

	class RSUFactory : public NodeFactory
	{
		// #rsu
		NodeContainer m_nodes;
		SharedPreference* m_setting;
	public:

		RSUFactory (SharedPreference* setting) 
		{
			m_setting = setting;
			m_nodes.Create (setting->num);
			m_setting->m_rsu_nodes= m_nodes;
			NS_LOG_DEBUG("create : " << setting->num << endl);
		}

		virtual void SetupApp ()
		{
			NS_LOG_DEBUG(" setup app " << endl);
		}
		void setup2RowMobility (MobilityHelper& m)
		{
			uint32_t grid_width = static_cast<double>(ceil(m_setting->num/2.0));
			m.SetPositionAllocator ("ns3::GridPositionAllocator",
			"MinX", DoubleValue (-30.0 * (grid_width-1)/2.0),
			"MinY", DoubleValue (-30), 
			"DeltaX", DoubleValue (30.0),
			"DeltaY", DoubleValue (60),
			"GridWidth", UintegerValue (grid_width),
			"LayoutType", StringValue ("RowFirst"));
		}
		virtual void SetupMobility ()
		{
			NS_LOG_DEBUG(" setup Mobility" << endl);
			MobilityHelper mobility;
			setup2RowMobility (mobility);
			mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install (m_nodes);

		}

		virtual void  SetupNetDev ()
		{
			NS_LOG_DEBUG(" setup Net Dev " << endl);
			ostringstream oss;
			NetDeviceContainer temp_dev;
			for (uint32_t i = 0; i < m_nodes.GetN (); ++i)
			{

				oss.str ("");
				oss << "wifi" << i;
				m_setting->m_wifiMac.SetType ("ns3::ApWifiMac",
				"Ssid", SsidValue (Ssid (oss.str ().c_str ())));

				// NS_LOG_UNCOND ("ssid : "<<oss.str ().c_str ());
				NetDeviceContainer dev_container = m_setting->m_wifi.Install (m_setting->m_wifiPhy , m_setting->m_wifiMac, m_nodes.Get (i));
				temp_dev.Add  (dev_container);
				Ptr<WifiNetDevice> wifi_dev = DynamicCast<WifiNetDevice> (dev_container.Get (0));
				Ptr<WifiPhy> wifi_phy = wifi_dev->GetPhy ();
				wifi_phy->SetChannelNumber(i+1);
			}

			

			//gw  setup
  			NodeContainer gw;
  			gw.Create (1);
  			///////////////////////////////////////
  			////// gateway mobility //////
  			///////////////////////////////////////
  			MobilityHelper mobility;
			Ptr<ListPositionAllocator> pos_gw = CreateObject<ListPositionAllocator> ();
			pos_gw->Add (Vector (-100, 50.0, 0.0));
			mobility.SetPositionAllocator(pos_gw);
			mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install (gw);

			CsmaHelper csma;
			csma.SetChannelAttribute ("DataRate", StringValue ("1Gbps"));
  			csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));

  			InternetStackHelper inet;
  			inet.Install (gw);
			inet.Install (m_nodes);
			
			m_setting->m_dev = temp_dev;


			///////////////////////////////////////////////////////////
			////////// for gw to rsu connection /////////////////////////
			//////////////////////////////////////////////////////////
			BridgeHelper bridge;  			
  			NetDeviceContainer gw_devs;
  			NetDeviceContainer rsu_csma_devs;

  			for (uint32_t i = 0; i < m_nodes.GetN (); ++i)
  			{
  				NetDeviceContainer rsu_to_gw_dev;
  				rsu_to_gw_dev = csma.Install (NodeContainer(m_nodes.Get (i), gw.Get (0)));
  				gw_devs.Add (rsu_to_gw_dev.Get (1));
  				rsu_csma_devs.Add (rsu_to_gw_dev.Get (0));
				
			      	bridge.Install (m_nodes.Get (i), NetDeviceContainer (m_setting->m_dev.Get (i), rsu_to_gw_dev.Get (0)));
	  			
  			}


		
			//////////////////////////////////////////////////////////
			////////// for gw to remote connection //////
			//////////////////////////////////////////////////////////
			NetDeviceContainer remote_devs;
		  	for (uint32_t i = 0; i < m_setting->remote_nodes.GetN (); ++i)
		  	{
		  		NetDeviceContainer gw_to_remote_dev;
		  		gw_to_remote_dev =  csma.Install (NodeContainer (gw.Get (0), m_setting->remote_nodes.Get (i)));
		  		remote_devs.Add (gw_to_remote_dev.Get (1));
		  		gw_devs.Add (gw_to_remote_dev.Get (0));
		  	}
		  	m_setting->m_ipv4.Assign (remote_devs);
		  	m_setting->m_ipv4.Assign (m_setting->m_dev);
		  	m_setting->m_ipv4.Assign (rsu_csma_devs);
  			m_setting->m_ipv4.Assign (gw_devs);
		  	bridge.Install (gw.Get (0), gw_devs);

		 //  	AsciiTraceHelper ascii;
  	// 		csma.EnableAsciiAll (ascii.CreateFileStream ("csma_remote.tr"));
			// csma.EnablePcapAll (std::string ("csma_remote"), true);


		  	
		 //  	BridgeHelper bridge;
			// NetDeviceContainer rsu_to_gw_bridge;
		 // //      	//p2p bridge csma
		 //      	rsu_to_gw_bridge = bridge.Install (m_nodes.Get (0), NetDeviceContainer (m_setting->m_dev.Get (0), rsu_to_gw_dev.Get (0)));
		  	
		 //  	NetDeviceContainer gw_to_remote_bridge;
		 //      	//p2p bridge csma
		 //      	gw_to_remote_bridge = bridge.Install (gw.Get (0), NetDeviceContainer (rsu_to_gw_dev.Get (1), csmaDevices.Get (0)));

		      	
  			
		 //      	// assign AP IP address to bridge, not wifi
			// m_setting->m_ipv4.Assign (csmaDevices);

		      	// assign remote first than assign for rsu
		      	// m_setting->m_ipv4.Assign (bridgeDev);

		      	
			

		}
		virtual void SetupIP ()
		{
			// NS_LOG_DEBUG(" setup IP " << endl);
			// InternetStackHelper inet;
			// inet.Install (m_nodes);
			// m_setting->m_ipv4.Assign (m_setting->m_dev);
		}
		void getApp ()
		{


		}
	};

	class ENodeBFactory : public NodeFactory
	{
		//#enodeb
		NodeContainer m_nodes;
		SharedPreference* m_setting;
		Ptr<LteHelper> m_lte_helper;
	public:

		ENodeBFactory (SharedPreference* setting) 
		{
			// NS_ASSERT_MSG (setting->num < 2, "the number of e node b shuould be less than 2");
			// NS_ASSERT_MSG (setting->num > 0, "the number of e node b shuould be bigger than 0");
			m_setting = setting;
			m_nodes.Create (setting->num);
			NS_LOG_DEBUG("create : " << setting->num << endl);
		}

		virtual void SetupApp ()
		{
			NS_LOG_DEBUG(" setup app " << endl);
		}
		virtual void SetupMobility ()
		{
			NS_LOG_DEBUG(" setup Mobility" << endl);
						MobilityHelper mobility;
			mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
			"MinX", DoubleValue (-100.0),
			"MinY", DoubleValue (-100.0), 
			"DeltaX", DoubleValue (-300.0),
			"DeltaY", DoubleValue (0.0),
			"GridWidth", UintegerValue (4),
			"LayoutType", StringValue ("RowFirst"));
			mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install (m_nodes);


		}
		virtual void  SetupNetDev ()
		{
			NS_LOG_DEBUG(" setup Net Dev " << endl);
			m_lte_helper = CreateObject<LteHelper> ();
			Ptr<PointToPointEpcHelper> epc_helper = CreateObject<PointToPointEpcHelper> ();
			m_lte_helper->SetEpcHelper (epc_helper);
			ConfigStore inputConfig;
			inputConfig.ConfigureDefaults ();
			m_setting->pgw_node = epc_helper->GetPgwNode ();		

			m_setting->m_enodeb_dev =  m_lte_helper->InstallEnbDevice (m_nodes);
			m_setting->m_vehicle_lte_dev = m_lte_helper->InstallUeDevice (m_setting->vehicle_nodes);

			PointToPointHelper p2ph;
			p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Mbps")));
			p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
			p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));

			for(uint32_t i = 0; i < m_setting->remote_nodes.GetN (); ++i)
			{
				m_setting->m_remote_dev.Add (p2ph.Install (m_setting->pgw_node, m_setting->remote_nodes.Get (i)));
			}
			m_setting->lte_helper = m_lte_helper;
			m_setting-> epc_helper = epc_helper;

		}
		virtual void SetupIP ()
		{
			NS_LOG_DEBUG(" setup IP " << endl);
			// InternetStackHelper inet;
			// inet.Install (m_nodes);
			// m_setting->m_ipv4.Assign (m_setting->m_dev);

		}
		void getApp ()
		{

		}
	};

	class PgwFactory : public NodeFactory
	{
		//#pgw
		NodeContainer m_nodes;
		SharedPreference* m_setting;
		Ipv4InterfaceContainer m_p2p_ipv4_container;
	public:

		PgwFactory (SharedPreference* setting) 
		{
			m_setting = setting;
			m_nodes.Add(m_setting->pgw_node);
			NS_LOG_DEBUG("create : " << setting->num << endl);
		}

		virtual void SetupApp ()
		{
			NS_LOG_DEBUG(" setup app " << endl);
			getLteApp ();
		}
		virtual void SetupMobility ()
		{
			NS_LOG_DEBUG(" setup Mobility" << endl);
			if (m_nodes.GetN() > 0)
			{	
				MobilityHelper mobility;
				Ptr<ListPositionAllocator> pos = CreateObject<ListPositionAllocator> ();
				pos->Add (Vector (-200.0, 100.0, 0.0));
				mobility.SetPositionAllocator(pos);
				mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
				mobility.Install (m_nodes);
			}
		}
		virtual void  SetupNetDev ()
		{
			NS_LOG_DEBUG(" setup Net Dev " << endl);

			
		}
		virtual void SetupIP ()
		{
			NS_LOG_DEBUG(" setup IP " << endl);
			Ipv4AddressHelper ipv4;
			NS_LOG_DEBUG ("remote dev number : " << m_setting->m_remote_dev.GetN () << endl);
			for (uint32_t i = 0; i < m_setting->m_remote_dev.GetN ()/2; ++i)
			{
				char buf[64];
				sprintf (buf, "1.0.%u.0", i);
				ipv4.SetBase (string(buf).c_str(), "255.255.255.0");
				m_p2p_ipv4_container.Add (ipv4.Assign (NetDeviceContainer(m_setting->m_remote_dev.Get (2 * i), m_setting->m_remote_dev.Get (2 * i +1))));
			}
			
			// 1.the ipv4 addr of odd index is belong to pgw, 
			// 2. the ipv4 addr of even index  is belong to remote host
			// m_p2p_ipv4_container = ipv4.Assign (m_setting->m_remote_dev);

			for (uint32_t i = 0; i < m_p2p_ipv4_container.GetN (); ++i)
			{
				NS_LOG_DEBUG ("addr " << i << " : " << m_p2p_ipv4_container.GetAddress (i));
			}
			Ipv4StaticRoutingHelper ipv4RoutingHelper;
			for (uint32_t i = 0; i < m_setting->remote_nodes.GetN (); ++i)
			{
				Ptr<Node> remote_host = m_setting->remote_nodes.Get (i);
				// 1.get the routing table of remote host 
				Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remote_host->GetObject<Ipv4> ());
				// 2.setup the static routing table to 7.0.0.0 and class A network at iface  2
				remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 2);

			}
			
			////////////////////////////////////////////////////////////
			// install the routing table in vehicles      ///
			////////////////////////////////////////////////////////////

			Ipv4InterfaceContainer ueIpIface;
			ueIpIface = m_setting->epc_helper->AssignUeIpv4Address (NetDeviceContainer (m_setting->m_vehicle_lte_dev));
			// Assign IP address to UEs, and install applications
			for (uint32_t u = 0; u < m_setting->m_vehicle_lte_dev.GetN (); ++u)
			{
				Ptr<Node> ueNode = m_setting->m_vehicle_lte_dev.Get (u)->GetNode ();
				// Set the default gateway for the UE
				Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
				ueStaticRouting->SetDefaultRoute (m_setting->epc_helper->GetUeDefaultGatewayAddress (), 2);
			}

			// Attach one UE per eNodeB
			uint32_t enb_num = m_setting->m_enodeb_dev.GetN ();
			for (uint32_t i = 0; i < m_setting->m_vehicle_lte_dev.GetN(); i++)
			{
				uint32_t target_enb = static_cast<uint32_t> (i % enb_num);
				m_setting->lte_helper->Attach (m_setting->m_vehicle_lte_dev.Get (i), m_setting->m_enodeb_dev.Get (target_enb));
			}


		}
		void getVehicleRecvApp ()
		{
			uint16_t sink_port = 7000;
			ApplicationContainer sink_apps;			
			for (uint32_t i = 0; i < m_setting->vehicle_nodes.GetN (); ++i)
			{
				uint32_t r_idx = static_cast<uint32_t> (static_cast<int>(i) % m_setting->vehicle_nodes.GetN ());

				NS_LOG_DEBUG ("GET LTE APP : ON THE VEHICLE " << i << " : "<<m_setting->vehicle_nodes.Get (r_idx));
				PacketSinkHelper pkt_sink_helper ("ns3::UdpSocketFactory", InetSocketAddress(Ipv4Address::GetAny (), sink_port+ static_cast<uint16_t>(i)));
				sink_apps.Add (pkt_sink_helper.Install (m_setting->vehicle_nodes.Get (r_idx)));
			}
			sink_apps.Start (Seconds (0.));
			sink_apps.Stop (Seconds (STOP_TIME));

		}
		void getVehicleSendApp ()
		{
			uint16_t sink_port = 9000;
			
			ApplicationContainer sender_apps;
			uint32_t sub_ip = 0;
			for (uint32_t i = 0; i < m_setting->vehicle_nodes.GetN (); ++i)
			{
				ostringstream sink_ip;
				sink_ip << "1.0." << sub_ip ++ << ".2";
				NS_LOG_DEBUG ("LTE sink : " << i <<":" << sink_ip.str ());
			
				Ptr<Socket> sock = Socket::CreateSocket (m_setting->vehicle_nodes.Get (i), UdpSocketFactory::GetTypeId ());
				Address sink_addr = InetSocketAddress (sink_ip.str ().c_str (), sink_port + static_cast<uint16_t>(i));
				Ptr<MyApp> app = CreateObject<MyApp> ();
				
				sender_apps.Add (app);
				app->Setup (sock, sink_addr, m_setting->m_payload, m_setting->m_pkts_num, DataRate (m_setting->m_send_rate.c_str ()));
				app->resumeApp ();
				m_setting->vehicle_nodes.Get (i)->AddApplication (app);
				app->SetStartTime (Seconds (1.));
				app->SetStopTime (Seconds (STOP_TIME));	
			}

		}				
		void getRemoteRecvApp ()
		{
			uint16_t sink_port = 9000;
			ApplicationContainer sink_apps;			
			for (uint32_t i = 0; i < m_setting->remote_nodes.GetN (); ++i)
			{
				uint32_t r_idx = static_cast<uint32_t> (static_cast<int>(i) % m_setting->remote_nodes.GetN ());

				NS_LOG_DEBUG ("GET LTE APP : ON THE VEHICLE " << i << " : "<<m_setting->remote_nodes.Get (r_idx));
				PacketSinkHelper pkt_sink_helper ("ns3::UdpSocketFactory", InetSocketAddress(Ipv4Address::GetAny (), sink_port+ static_cast<uint16_t>(i)));
				sink_apps.Add (pkt_sink_helper.Install (m_setting->remote_nodes.Get (r_idx)));
			}
			sink_apps.Start (Seconds (0.));
			sink_apps.Stop (Seconds (STOP_TIME));

		}
		void getRemoteSendApp ()
		{
			uint16_t sink_port = 7000;
			
			ApplicationContainer sender_apps;
			uint32_t sub_ip = 1+ 1;
			for (uint32_t i = 0; i < m_setting->remote_nodes.GetN (); ++i)
			{
				ostringstream sink_ip;
				sink_ip << "7.0.0." << sub_ip ++;
				NS_LOG_DEBUG ("LTE sink : " << i <<":" << sink_ip.str ());
			
				Ptr<Socket> sock = Socket::CreateSocket (m_setting->remote_nodes.Get (i), UdpSocketFactory::GetTypeId ());
				Address sink_addr = InetSocketAddress (sink_ip.str ().c_str (), sink_port + static_cast<uint16_t>(i));
				Ptr<MyApp> app = CreateObject<MyApp> ();
				
				sender_apps.Add (app);
				app->Setup (sock, sink_addr, m_setting->m_payload, m_setting->m_pkts_num, DataRate (SEND_RATE));
				app->resumeApp ();
				m_setting->remote_nodes.Get (i)->AddApplication (app);
				app->SetStartTime (Seconds (1.));
				app->SetStopTime (Seconds (STOP_TIME));	
			}

		}				
		void getVehicleApp ()
		{
			// getVehicleRecvApp ();
			getVehicleSendApp ();

		}
		void getRemoteApp ()
		{
			getRemoteRecvApp ();
			// getRemoteSendApp ();

		}
		void getLteApp ()
		{
			getVehicleApp ();
			getRemoteApp ();
		}
	};

	class ProbeNodeFactory : public NodeFactory
	{
		// #probe node
		NodeContainer m_nodes;
		SharedPreference* m_setting;
		Ipv4InterfaceContainer ipv4_addr;
	public:

		ProbeNodeFactory	 (SharedPreference* setting) 
		{
			m_setting = setting;
			m_nodes.Create (setting->num);
			NS_LOG_DEBUG("create : " << setting->num << endl);
		}

		virtual void SetupApp ()
		{
			NS_LOG_DEBUG(" setup app " << endl);
			
			uint16_t sink_port = 6000;
			for (uint32_t i = 0; i < m_nodes.GetN ()/2; ++i)
			{
				/* code */
				Address remote_addr (InetSocketAddress (Ipv4Address::GetAny (), sink_port));
				PacketSinkHelper pkt_sinker ("ns3::UdpSocketFactory", remote_addr);
				
				ApplicationContainer apps = pkt_sinker.Install (m_nodes.Get (2 * i));
				apps.Start (Seconds (0.0));
				apps.Stop (Seconds (STOP_TIME));
				NS_LOG_UNCOND ( "probe node recv ip : " << ipv4_addr.GetAddress (i));
				OnOffHelper onoff ("ns3::UdpSocketFactory", Address (InetSocketAddress (ipv4_addr.GetAddress (2 * i), sink_port)));

				onoff.SetConstantRate (DataRate (SEND_RATE));
				onoff.SetAttribute ("OnTime",  StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
				onoff.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

				onoff.SetAttribute ("PacketSize", UintegerValue (m_setting->m_payload));
				// onoff.SetAttribute ("Remote", AddressValue (InetSocketAddress (/, sink_port)));
				
				ApplicationContainer sender_apps = onoff.Install (m_nodes.Get (2 * i + 1));
				sender_apps.Start (Seconds (1.0));
				sender_apps.Stop (Seconds (STOP_TIME));

			}


		}
		virtual void SetupMobility ()
		{
			NS_LOG_DEBUG(" setup Mobility" << endl);
			MobilityHelper mobility;
			mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
			"MinX", DoubleValue (-45.0),
			"MinY", DoubleValue (-45.0),
			"DeltaX", DoubleValue (30.0),
			"DeltaY", DoubleValue (30.0),
			"GridWidth", UintegerValue (4),
			"LayoutType", StringValue ("RowFirst"));
			mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install (m_nodes);
		}
		
		virtual void  SetupNetDev ()
		{
			NS_LOG_DEBUG(" setup Net Dev " << endl);
			m_setting->m_dev = m_setting->m_wifi.Install (m_setting->m_wifiPhy, m_setting->m_wifiMac, m_nodes);
		}
		virtual void SetupIP ()
		{
			NS_LOG_DEBUG(" setup IP " << endl);
			InternetStackHelper inet;
			inet.Install (m_nodes);
			ipv4_addr = m_setting->m_ipv4.Assign (m_setting->m_dev);
		}	
	};
#endif