#ifndef PROCESS_DATA_H
#define PROCESS_DATA_H
#include <iostream>
#include <cstdlib>
#include <vector>
#include <sstream>

using namespace std;


string dumpDoubleData (vector<double> vec)
{
	// bool all_unchanged = check_vector_init (vec);
	bool all_unchanged = false;
	if (all_unchanged)
	{
		return "no data";
	}
	else 
	{
		int last_index = vec.end () - vec.begin ();
		ostringstream oss;
		for (vector<double>::iterator it = vec.begin (); it != vec.end ()  && it - vec.begin () <= last_index; ++it)
		{
			oss << *it << ",";
		}
		return  oss.str (). c_str ();
	}
}
class ProcessLog 
{
	public :
		ProcessLog ()
		{

		}
		static vector<double> groupSamplingPoint (std::vector<double> v, int group_unit_size)
		{
			int times = static_cast<int> (v.size () / group_unit_size) + (v.size () % group_unit_size == 0? 0 : 1);
			std::vector<double> result;

			for (int i = 0; i < times; ++i)
			{
				int end = ((i + 1) * group_unit_size < static_cast <int> (v.size ()) ? (i + 1) * group_unit_size : static_cast <int>  (v.size ()));
				std::vector<double> target_range ((v.begin () + i * group_unit_size), v.begin () + end);
				result.push_back (average (target_range));
			}
			return result;
		}
		static double average (std::vector<double> v)
		{
			if (v.size () == 0)
			{
				return 0.0;
			}
			else
			{
				return static_cast<double> (sum (v) )/v.size () ;
			}
		}

		static double sum (std::vector<double> v)
		{
			double result = 0;
			for (std::vector<double>::iterator it = v.begin(); it != v.end(); ++it)
			{
				result += *it;
			}
			return result;
		}

};

#endif